use crate::EngineThreadMessage;
use bevy::prelude::*;
use std::sync::mpsc;
use vismut_core::prelude::Engine;

pub struct RetrieveEngine;
impl Plugin for RetrieveEngine {
    fn build(&self, app: &mut App) {
        app.add_system_to_stage(
            CoreStage::Update,
            retrieve_engine.exclusive_system().at_start(),
        );
    }
}

fn retrieve_engine(world: &mut World) {
    if world.get_resource::<Engine>().is_some() {
        return;
    }

    let sender = world
        .remove_non_send_resource::<mpsc::Sender<EngineThreadMessage>>()
        .expect("sender should always exist");
    let receiver = world
        .remove_non_send_resource::<mpsc::Receiver<Engine>>()
        .expect("receiver should always exist");

    sender.send(EngineThreadMessage::ReturnEngine).unwrap();
    let engine = receiver.recv().unwrap();

    world.insert_resource(engine);
    world.insert_non_send_resource(sender);
    world.insert_non_send_resource(receiver);
}
