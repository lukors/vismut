// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

mod backplate;
mod points;
mod snap;

use crate::PostUndoCommandStage;
use bevy::prelude::*;
use points::GridParent;

/// This plugin renders the grid, and also contains the system that moves nodes to a grid-snapped
/// location when their `GridPosition` is updated.
pub struct GridPlugin;
impl Plugin for GridPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup).add_system_set_to_stage(
            PostUndoCommandStage,
            SystemSet::new()
                .with_system(snap::update_translation)
                .with_system(snap::update_edges.after(snap::update_translation))
                .with_system(backplate::update_backplate)
                .with_system(points::move_grid)
                .with_system(points::resize_grid),
        );
    }
}

fn setup(mut commands: Commands) {
    commands
        .spawn()
        .insert(GridParent)
        .insert(Transform::from_translation(Vec3::Z))
        .insert(GlobalTransform::default());
}
