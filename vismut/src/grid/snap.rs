use crate::{stretch_between, Dragged, GridPosition, GuiEdge, SlotAddressSideComponent, Workspace};

use crate::shared::{NodeAddressComponent, SnapToGrid, BACKPLATE_PADDING, GRID_HEIGHT, GRID_WIDTH};
use bevy::prelude::*;
use vismut_core::prelude::*;

/// Snaps grid-positioned entities with a `SnapToGrid` component to the grid.
pub(super) fn update_translation(
    mut commands: Commands,
    mut q_node: Query<(Entity, &mut Transform, &GridPosition), With<SnapToGrid>>,
) {
    for (entity, mut transform, grid_position) in q_node.iter_mut() {
        transform.translation.x =
            (grid_position.x * GRID_WIDTH as i32 + BACKPLATE_PADDING as i32) as f32;
        transform.translation.y =
            (grid_position.y * GRID_HEIGHT as i32 - BACKPLATE_PADDING as i32) as f32;

        commands.entity(entity).remove::<SnapToGrid>();
    }
}

/// Updates all the edges connected to nodes that have been moved.
pub(super) fn update_edges(
    workspace: Res<Workspace>,
    q_node: Query<
        (&NodeAddressComponent, &Transform, Option<&Dragged>),
        Or<(With<Dragged>, Changed<Transform>)>,
    >,
    q_slots: Query<(&SlotAddressSideComponent, &Transform)>,
    mut q_edge: Query<
        (&mut Sprite, &mut Transform, &mut GuiEdge),
        (
            Without<NodeAddressComponent>,
            Without<SlotAddressSideComponent>,
        ),
    >,
) {
    for (mut sprite, mut edge_transform, mut gui_edge) in q_edge.iter_mut() {
        let mut found_change = false;

        for (node_address_component, node_transform, dragged) in q_node.iter() {
            if gui_edge.input_slot_address.without_slot_id() == **node_address_component
                || gui_edge.output_slot_address.without_slot_id() == **node_address_component
            {
                // Update the position of the start or end point of the edge.
                for (slot_address_side_component, slot_transform) in
                    q_slots.iter().filter(|(slot_address_side_component, _)| {
                        slot_address_side_component.node_id == node_address_component.node_id()
                    })
                {
                    if **slot_address_side_component
                        == gui_edge.output_slot_address.with_side(Side::Output)
                    {
                        gui_edge.start =
                            (node_transform.translation + slot_transform.translation).truncate();
                        if dragged.is_some() {
                            gui_edge.start += workspace.cursor_world;
                        }

                        found_change = true;
                        break;
                    } else if **slot_address_side_component
                        == gui_edge.input_slot_address.with_side(Side::Input)
                    {
                        gui_edge.end =
                            (node_transform.translation + slot_transform.translation).truncate();
                        if dragged.is_some() {
                            gui_edge.end += workspace.cursor_world;
                        }

                        found_change = true;
                        break;
                    }
                }
            }
        }

        // Todo: Do this stretching in its own system on any changed edges.
        if found_change {
            stretch_between(
                &mut sprite,
                &mut edge_transform,
                gui_edge.start,
                gui_edge.end,
            );
        }
    }
}
