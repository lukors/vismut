use crate::shared::{BACKPLATE_PADDING, GRID_HEIGHT, GRID_WIDTH};
use crate::GridSize;
use bevy::prelude::*;

/// Updates the size of any "backplates" that has changed. The backplate is the sprite that forms
/// the "foundation" of for instance a node.
pub(super) fn update_backplate(
    mut q_backplate: Query<(&GridSize, &mut Sprite), Changed<GridSize>>,
) {
    for (grid_size, mut sprite) in q_backplate.iter_mut() {
        if let Some(ref mut size) = sprite.custom_size {
            let width: u8 = grid_size.width.into();
            let size_x: usize = GRID_WIDTH * width as usize - BACKPLATE_PADDING * 2;
            size.x = size_x as f32;

            let height: u8 = grid_size.height.into();
            let size_y: usize = GRID_HEIGHT * height as usize - BACKPLATE_PADDING * 2;
            size.y = size_y as f32;
        }
    }
}
