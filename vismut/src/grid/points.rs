use crate::shared::{GRID_HEIGHT, GRID_POINT_SIZE, GRID_WIDTH};
use crate::{
    default, BuildChildren, ChildBuilder, Color, Commands, DespawnRecursiveExt, Entity,
    EventReader, Quat, Query, Res, Sprite, SpriteBundle, Transform, Vec2, Vec3, Windows, With,
    Without, WorkspaceCamera,
};
use bevy::{prelude::*, window::WindowResized};
use std::f32::consts::PI;

#[derive(Component, Default)]
pub(super) struct GridPoint;

#[derive(Component, Default)]
pub(super) struct GridParent;

#[derive(Bundle, Default)]
struct GridPointBundle {
    grid_point: GridPoint,
    #[bundle]
    sprite_bundle: SpriteBundle,
}

/// Moves the grid of dots when the camera has moved, so that the grid always stays in view.
pub(super) fn move_grid(
    windows: Res<Windows>,
    mut q_grid_parent: Query<&mut Transform, (With<GridParent>, Without<WorkspaceCamera>)>,
    q_camera: Query<(&Transform, ChangeTrackers<Transform>), With<WorkspaceCamera>>,
    window_resized_events: EventReader<WindowResized>,
) {
    /// Moves the grid on a single axis.
    fn update_single_axis(camera_position: f32, window_size: f32) -> f32 {
        // Get edge of camera in world space.
        ((camera_position - window_size / 2.0)
            // Divide by `GRID_WIDTH` so it can be rounded to closest grid point.
            / GRID_WIDTH as f32)
            // Perform rounding to snap it to closest grid point.
            .round()
            // Multiply back up to "real" size.
            * GRID_WIDTH as f32
            // Add half grid size to center them
            + GRID_WIDTH as f32 / 2.0
    }

    let (camera_transform, camera_tracker) = q_camera.single();

    if camera_tracker.is_changed() || !window_resized_events.is_empty() {
        let window = windows.get_primary().unwrap();
        let mut transform = q_grid_parent.single_mut();

        transform.translation.x =
            update_single_axis(camera_transform.translation.x, window.width());
        transform.translation.y =
            update_single_axis(camera_transform.translation.y, window.height());
    }
}

pub(super) fn resize_grid(
    mut commands: Commands,
    mut window_resized_events: EventReader<WindowResized>,
    q_grid_parent: Query<Entity, With<GridParent>>,
    mut q_grid_points: Query<(Entity, &mut Transform), With<GridPoint>>,
) {
    if let Some(window_resized_event) = window_resized_events.iter().last() {
        let grid_parent_entity = q_grid_parent.single();
        let mut grid_points = q_grid_points.iter_mut();

        let x_count = (window_resized_event.width / GRID_WIDTH as f32).round() as usize;
        let y_count = (window_resized_event.height / GRID_HEIGHT as f32).round() as usize;

        for x in 0..=x_count {
            for y in 0..=y_count {
                if let Some((_, mut grid_point_transform)) = grid_points.next() {
                    // If a grid point already exists, move it to its new location for the new
                    // window size.
                    grid_point_transform.translation.x = (x * GRID_WIDTH) as f32;
                    grid_point_transform.translation.y = (y * GRID_HEIGHT) as f32;
                } else {
                    // If a grid point doesn't already exist, create a new one. This happens if the
                    // window was made larger.
                    commands.entity(grid_parent_entity).with_children(|parent| {
                        spawn_point(parent, x, y);
                    });
                }
            }
        }

        // If the window was made smaller, remove any grid points that don't fit in the new window
        // size.
        for (entity, ..) in grid_points {
            commands.entity(entity).despawn_recursive();
        }
    }
}

fn spawn_point(child_builder: &mut ChildBuilder, x: usize, y: usize) {
    child_builder.spawn_bundle(GridPointBundle {
        sprite_bundle: SpriteBundle {
            sprite: Sprite {
                color: Color::hsl(0.0, 0.0, 0.2),
                custom_size: Some(Vec2::new(GRID_POINT_SIZE as f32, GRID_POINT_SIZE as f32)),
                ..default()
            },
            transform: Transform {
                translation: Vec3::new((x * GRID_WIDTH) as f32, (y * GRID_HEIGHT) as f32, -1.0),
                rotation: Quat::from_rotation_z(PI / 4.0),
                ..default()
            },
            ..default()
        },
        ..default()
    });
}
