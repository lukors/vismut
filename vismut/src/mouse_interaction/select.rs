// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.
use crate::{
    shared::NodeAddressComponent,
    sync_graph::GuiEdge,
    undo::{prelude::*, undo_command_manager::BoxUndoCommand, UndoCommand, UndoCommandType},
};
use bevy::prelude::*;
use vismut_core::prelude::*;

use super::{active::MakeItemNotActive, Selection};

#[derive(Component, Default)]
pub(crate) struct Selected;

fn set_node_selection(world: &mut World, node_address: NodeAddress, selected: bool) {
    let q_node = if selected {
        world
            .query_filtered::<(Entity, &NodeAddressComponent), Without<Selected>>()
            .iter(world)
            .find(|(_, node_address_component)| node_address_component.0 == node_address)
    } else {
        world
            .query_filtered::<(Entity, &NodeAddressComponent), With<Selected>>()
            .iter(world)
            .find(|(_, node_address_component)| node_address_component.0 == node_address)
    };

    if let Some((entity, _)) = q_node {
        if selected {
            world.entity_mut(entity).insert(Selected);
        } else {
            world.entity_mut(entity).remove::<Selected>();
        }
    } else {
        warn!(
            "failed to {} a node",
            if selected { "select" } else { "deselect" }
        );
    }
}

fn set_edge_selection(world: &mut World, edge_address: GuiEdge, selected: bool) {
    let q_edge_id = if selected {
        world
            .query_filtered::<(Entity, &GuiEdge), Without<Selected>>()
            .iter(world)
            .find(|(_, edge_address_component)| **edge_address_component == edge_address)
    } else {
        world
            .query_filtered::<(Entity, &GuiEdge), With<Selected>>()
            .iter(world)
            .find(|(_, edge_address_component)| **edge_address_component == edge_address)
    };

    if let Some((entity, _)) = q_edge_id {
        if selected {
            world.entity_mut(entity).insert(Selected);
        } else {
            world.entity_mut(entity).remove::<Selected>();
        }
    } else {
        warn!(
            "failed to {} a edge",
            if selected { "select" } else { "deselect" }
        );
    }
}

fn select_item(world: &mut World, selectable: Selection) {
    match selectable {
        Selection::Node(n) => set_node_selection(world, n, true),
        Selection::Edge(e) => set_edge_selection(world, e, true),
    }
}
fn deselect_item(world: &mut World, selectable: Selection) {
    match selectable {
        Selection::Node(n) => set_node_selection(world, n, false),
        Selection::Edge(e) => set_edge_selection(world, e, false),
    }
}

#[derive(Debug)]
pub struct ReplaceSelection(pub Vec<Selection>);
impl UndoCommand for ReplaceSelection {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        info!("ReplaceSelection {:?}", self.0);
        let mut q_node_id = world.query::<(&NodeAddressComponent, Option<&Selected>)>();
        let mut q_edge = world.query::<(&GuiEdge, Option<&Selected>)>();

        for (node_address, selected) in q_node_id.iter(world) {
            let in_new_selection = self.0.contains(&Selection::Node(node_address.0));

            if selected.is_none() && in_new_selection {
                undo_command_manager
                    .commands
                    .push_front(Box::new(SelectItemOnly(Selection::Node(node_address.0))));
            } else if selected.is_some() && !in_new_selection {
                undo_command_manager
                    .commands
                    .push_front(Box::new(DeselectItemOnly(Selection::Node(node_address.0))));
            }
        }
        for (edge, selected) in q_edge.iter(world) {
            let in_new_selection = self.0.contains(&Selection::Edge(*edge));

            if selected.is_none() && in_new_selection {
                undo_command_manager
                    .commands
                    .push_front(Box::new(SelectItemOnly(Selection::Edge(*edge))));
            } else if selected.is_some() && !in_new_selection {
                undo_command_manager
                    .commands
                    .push_front(Box::new(DeselectItemOnly(Selection::Edge(*edge))));
            }
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("command is never put on undo stack");
    }
}

//
// Selecting
//

#[derive(Copy, Clone, Debug)]
struct SelectItemOnly(Selection);
impl UndoCommand for SelectItemOnly {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        select_item(world, self.0)
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        deselect_item(world, self.0);
    }
}

#[derive(Copy, Clone, Debug)]
pub struct SelectItem(pub Selection);
impl UndoCommand for SelectItem {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_node_id = world.query_filtered::<&NodeAddressComponent, Without<Selected>>();
        let mut q_edge_id = world.query_filtered::<&GuiEdge, Without<Selected>>();
        info!("Select Node {:?}", self.0);
        if q_node_id
            .iter(world)
            .any(|node_id| Selection::Node(node_id.0) == self.0)
            || q_edge_id
                .iter(world)
                .any(|edge| Selection::Edge(*edge) == self.0)
        {
            undo_command_manager.push_front(Box::new(SelectItemOnly(self.0)));
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is never put on the undo stack");
    }
}

//
// Deselecting
//

#[derive(Copy, Clone, Debug)]
struct DeselectItemOnly(Selection);
impl UndoCommand for DeselectItemOnly {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        deselect_item(world, self.0);
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        select_item(world, self.0);
    }
}

#[derive(Copy, Clone, Debug)]
pub struct DeselectItem(pub Selection);
impl UndoCommand for DeselectItem {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_node_id = world.query_filtered::<&NodeAddressComponent, With<Selected>>();
        let mut q_edge = world.query_filtered::<&GuiEdge, With<Selected>>();

        if q_node_id
            .iter(world)
            .any(|node_id| Selection::Node(node_id.0) == self.0)
            || q_edge
                .iter(world)
                .any(|edge| Selection::Edge(*edge) == self.0)
        {
            let undo_batch: Vec<BoxUndoCommand> = vec![
                Box::new(MakeItemNotActive(self.0)),
                Box::new(DeselectItemOnly(self.0)),
            ];
            undo_command_manager.push_front_vec(undo_batch);
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is never put on the undo stack");
    }
}

#[derive(Copy, Clone, Debug)]
pub struct DeselectAll;
impl UndoCommand for DeselectAll {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_selected = world.query_filtered::<&NodeAddressComponent, With<Selected>>();
        let mut q_selected_edge = world.query_filtered::<&GuiEdge, With<Selected>>();

        for node_id in q_selected.iter(world) {
            undo_command_manager
                .commands
                .push_front(Box::new(DeselectItemOnly(Selection::Node(node_id.0))));
        }
        for edge_id in q_selected_edge.iter(world) {
            undo_command_manager
                .commands
                .push_front(Box::new(DeselectItemOnly(Selection::Edge(*edge_id))));
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("command is never put on undo stack");
    }
}

#[derive(Copy, Clone, Debug)]
pub struct SelectAll;
impl UndoCommand for SelectAll {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_nodes_not_selected =
            world.query_filtered::<&NodeAddressComponent, Without<Selected>>();

        for node_id in q_nodes_not_selected.iter(world) {
            undo_command_manager
                .commands
                .push_front(Box::new(SelectItemOnly(Selection::Node(node_id.0))));
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("command is never put on undo stack");
    }
}
