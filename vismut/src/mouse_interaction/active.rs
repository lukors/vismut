// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::{
    mouse_interaction::select::SelectItem, shared::NodeAddressComponent, sync_graph::GuiEdge,
    undo::prelude::*,
};
use bevy::prelude::*;
use vismut_core::prelude::*;

use super::Selection;

#[derive(Component, Default)]
pub(crate) struct Active;

fn set_node_active(world: &mut World, node_address: NodeAddress, active: bool) {
    let q_node_id = if active {
        world
            .query_filtered::<(Entity, &NodeAddressComponent), Without<Active>>()
            .iter(world)
            .find(|(_, node_id_component)| node_id_component.0 == node_address)
    } else {
        world
            .query_filtered::<(Entity, &NodeAddressComponent), With<Active>>()
            .iter(world)
            .find(|(_, node_id_component)| node_id_component.0 == node_address)
    };

    if let Some((entity, _)) = q_node_id {
        if active {
            world.entity_mut(entity).insert(Active);
        } else {
            world.entity_mut(entity).remove::<Active>();
        }
    } else {
        warn!(
            "failed to make a node {}",
            if active { "active" } else { "inactive" }
        );
    }
}

fn set_edge_active(world: &mut World, edge_address: GuiEdge, active: bool) {
    let q_edge_id = if active {
        world
            .query_filtered::<(Entity, &GuiEdge), Without<Active>>()
            .iter(world)
            .find(|(_, edge_id_component)| **edge_id_component == edge_address)
    } else {
        world
            .query_filtered::<(Entity, &GuiEdge), With<Active>>()
            .iter(world)
            .find(|(_, edge_id_component)| **edge_id_component == edge_address)
    };

    if let Some((entity, _)) = q_edge_id {
        if active {
            world.entity_mut(entity).insert(Active);
        } else {
            world.entity_mut(entity).remove::<Active>();
        }
    } else {
        warn!(
            "failed to make an edge {}",
            if active { "active" } else { "inactive" }
        );
    }
}

fn make_item_active(world: &mut World, item: Selection) {
    match item {
        Selection::Node(n) => set_node_active(world, n, true),
        Selection::Edge(e) => set_edge_active(world, e, true),
    }
}
fn make_item_not_active(world: &mut World, item: Selection) {
    match item {
        Selection::Node(n) => set_node_active(world, n, false),
        Selection::Edge(e) => set_edge_active(world, e, false),
    }
}

//
// Making nodes active
//

#[derive(Copy, Clone, Debug)]
struct MakeItemActiveOnly(pub Selection);
impl UndoCommand for MakeItemActiveOnly {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        match self.0 {
            Selection::Node(n) => set_node_active(world, n, true),
            Selection::Edge(e) => set_edge_active(world, e, true),
        }
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        match self.0 {
            Selection::Node(n) => set_node_active(world, n, false),
            Selection::Edge(e) => set_edge_active(world, e, false),
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct MakeItemActive(pub Selection);
impl UndoCommand for MakeItemActive {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_active_node_id = world.query_filtered::<&NodeAddressComponent, With<Active>>();
        let mut q_active_edge_id = world.query_filtered::<&GuiEdge, With<Active>>();
        assert!(
            q_active_node_id.iter(world).count() + q_active_edge_id.iter(world).count() < 2,
            "there is more than one active node or edge"
        );

        let mut undo_batch: Vec<BoxUndoCommand> = {
            if let Some(active_node_id) = q_active_node_id.iter(world).next() {
                if Selection::Node(active_node_id.0) != self.0 {
                    vec![Box::new(MakeItemNotActive(Selection::Node(
                        active_node_id.0,
                    )))]
                } else {
                    Vec::new()
                }
            } else if let Some(active_edge_id) = q_active_edge_id.iter(world).next() {
                if Selection::Edge(*active_edge_id) != self.0 {
                    vec![Box::new(MakeItemNotActive(Selection::Edge(
                        *active_edge_id,
                    )))]
                } else {
                    Vec::new()
                }
            } else {
                Vec::new()
            }
        };
        undo_batch.push(Box::new(SelectItem(self.0)));
        undo_batch.push(Box::new(MakeItemActiveOnly(self.0)));

        undo_command_manager.push_front_vec(undo_batch);
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is never put on the undo stack");
    }
}

//
// Making nodes not active
//

#[derive(Copy, Clone, Debug)]
struct MakeItemNotActiveOnly(pub Selection);
impl UndoCommand for MakeItemNotActiveOnly {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        make_item_not_active(world, self.0);
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        make_item_active(world, self.0);
    }
}

#[derive(Copy, Clone, Debug)]
pub struct MakeItemNotActive(pub Selection);
impl UndoCommand for MakeItemNotActive {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_active_node_id = world.query_filtered::<&NodeAddressComponent, With<Active>>();
        let mut q_active_edge = world.query_filtered::<&GuiEdge, With<Active>>();
        assert!(
            q_active_node_id.iter(world).count() + q_active_edge.iter(world).count() <= 1,
            "there is more than one active item"
        );

        match self.0 {
            Selection::Node(n) => {
                if let Some(active_node_id) = q_active_node_id.iter(world).next() {
                    if active_node_id.0 == n {
                        undo_command_manager.push_front(Box::new(MakeItemNotActiveOnly(self.0)));
                    } else {
                        warn!("tried making a not active node not active");
                    }
                } else {
                    warn!("could not find an active node to make not active");
                }
            }
            Selection::Edge(e) => {
                if let Some(active_edge) = q_active_edge.iter(world).next() {
                    if *active_edge == e {
                        undo_command_manager.push_front(Box::new(MakeItemNotActiveOnly(self.0)));
                    } else {
                        warn!("tried making a not active edge not active");
                    }
                } else {
                    warn!("could not find an active edge to make not active");
                }
            }
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is never put on the undo stack");
    }
}

#[derive(Copy, Clone, Debug)]
pub struct MakeNothingActive;
impl UndoCommand for MakeNothingActive {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_active_node_id = world.query_filtered::<&NodeAddressComponent, With<Active>>();
        let mut q_active_edge_id = world.query_filtered::<&GuiEdge, With<Active>>();
        assert!(
            q_active_node_id.iter(world).count() + q_active_edge_id.iter(world).count() <= 1,
            "there is more than one active item"
        );

        if let Some(node_address) = q_active_node_id.iter(world).next() {
            undo_command_manager
                .push_front(Box::new(MakeItemNotActive(Selection::Node(node_address.0))));
        } else if let Some(edge_address) = q_active_edge_id.iter(world).next() {
            undo_command_manager
                .push_front(Box::new(MakeItemNotActive(Selection::Edge(*edge_address))));
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this undo command is never put on the undo stack");
    }
}
