// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;
use vismut_core::prelude::*;

use crate::sync_graph::GuiEdge;
use crate::thumbnail::ThumbnailReceiver;
use crate::{
    mouse_interaction::active::Active,
    shared::{NodeAddressComponent, NodeStateComponent, SlotTypeComponent},
    sync_graph::{NodeFrame, SlotFrame},
    // thumbnail::ThumbnailState,
    Hovered,
    Selected,
};

pub const HUE_AQUA: f32 = 161.0;
pub const HUE_GOLD: f32 = 35.0;
pub const HUE_RED: f32 = 0.0;

pub(crate) struct MaterialPlugin;
impl Plugin for MaterialPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set_to_stage(
            CoreStage::Update,
            SystemSet::new()
                .with_system(update_node_state)
                .with_system(node_state_materials.after(update_node_state))
                .with_system(slot_hover)
                .with_system(node_hover)
                .with_system(edge_hover),
        );
    }
}

fn update_node_state(
    engine: Res<Engine>,
    dag_id: Res<DagId>,
    mut q_node: Query<(&NodeAddressComponent, &mut NodeStateComponent)>,
) {
    let node_states = engine.node_states(*dag_id);

    for (node_address, mut node_state) in q_node.iter_mut() {
        let new_state = node_states
            .get(&node_address.node_id())
            .expect("there should always be a state");

        **node_state = *new_state;
    }
}

fn node_state_materials(
    mut q_node: Query<(Entity, &mut Sprite, &NodeStateComponent)>,
    q_thumbnail: Query<&Parent, With<ThumbnailReceiver>>,
) {
    for (entity, mut sprite, node_state) in q_node.iter_mut() {
        const PROCESSING: Color = Color::hsl(HUE_AQUA, 0.5, 0.3);

        let color = match **node_state {
            NodeState::Dirty => Color::hsl(HUE_RED, 0.5, 0.3),
            NodeState::Processing => PROCESSING,
            NodeState::Clean => {
                let thumbnail_processing = q_thumbnail.iter().any(|parent| parent.0 == entity);

                if thumbnail_processing {
                    PROCESSING
                } else {
                    Color::rgb(0.27, 0.27, 0.27)
                }
            }
        };

        sprite.color = color;
    }
}

const ACTIVE_COLOR: Color = Color::hsl(HUE_AQUA, 0.6, 0.45);
const HOVER_COLOR: Color = Color::rgb(0.5, 0.5, 0.5);
const SELECTED_COLOR: Color = Color::rgb(0.8, 0.8, 0.8);

fn node_hover(
    q_node: Query<
        (Entity, Option<&Hovered>, Option<&Selected>, Option<&Active>),
        With<NodeAddressComponent>,
    >,
    mut q_frame: Query<
        (&Parent, &mut Sprite, &mut Visibility),
        (With<NodeFrame>, Without<NodeAddressComponent>),
    >,
) {
    for (entity, hovered, selected, active) in q_node.iter() {
        if let Some((_, mut sprite, mut frame_visibility)) =
            q_frame.iter_mut().find(|(parent, ..)| parent.0 == entity)
        {
            let (visibility, color) = if active.is_some() {
                (true, ACTIVE_COLOR)
            } else if selected.is_some() {
                (true, SELECTED_COLOR)
            } else if hovered.is_some() {
                (true, HOVER_COLOR)
            } else {
                (false, Color::BLACK)
            };

            sprite.color = color;
            frame_visibility.is_visible = visibility;
        }
    }
}

fn slot_hover(
    q_slot: Query<(Entity, Option<&Hovered>), With<SlotTypeComponent>>,
    mut q_frame: Query<(&Parent, &mut Sprite), With<SlotFrame>>,
) {
    for (entity, hovered) in q_slot.iter() {
        if let Some((_, mut sprite)) = q_frame.iter_mut().find(|(parent, ..)| parent.0 == entity) {
            let value = if hovered.is_some() { 1.0 } else { 0.0 };

            let color = Color::hsl(0.0, 0.0, value);

            sprite.color = color;
        }
    }
}

pub(super) fn edge_hover(
    mut q_edge_color: Query<(&mut Sprite, Option<&Hovered>, Option<&Selected>), With<GuiEdge>>,
) {
    for (mut sprite, hovered, selected) in q_edge_color.iter_mut() {
        sprite.color = match (hovered.is_some(), selected.is_some()) {
            (_, true) => ACTIVE_COLOR,
            (true, false) => HOVER_COLOR,
            _ => Color::BLACK,
        };
    }
}

pub fn slot_type_to_color(slot_type: SlotType, lightness: f32) -> Color {
    match slot_type {
        SlotType::Gray => {
            let gray_slot = 0.9;
            Color::rgb(gray_slot, gray_slot, gray_slot)
        }
        SlotType::Rgba => Color::hsl(HUE_GOLD, 1.0, lightness),
        // SlotType::GrayOrRgba => Color::rgb(0.6, 1.0, 0.8),
    }
}
