// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::shared::NodeAddressComponent;
use crate::{EngineThreadMessage, PostUndoCommandStage};
use bevy::prelude::*;
use std::sync::mpsc;
use std::{collections::VecDeque, fmt::Debug};
use vismut_core::prelude::*;

use super::{UndoCommand, UndoCommandType};

#[derive(Component)]
pub struct Dirtied;

pub type BoxUndoCommand = Box<dyn UndoCommand + Send + Sync + 'static>;

#[derive(Debug, Default)]
pub struct UndoCommandManager {
    pub(crate) commands: VecDeque<BoxUndoCommand>,
    pub(crate) undo_stack: Vec<BoxUndoCommand>,
    pub(crate) redo_stack: Vec<BoxUndoCommand>,
    // Maybe this and also the undo/redo stacks should be made private with some refactoring?
    pub(crate) command_batch: VecDeque<BoxUndoCommand>,
}

impl UndoCommandManager {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn clear(&mut self) {
        self.commands.clear();
        self.command_batch.clear();
        self.undo_stack.clear();
        self.redo_stack.clear();
    }

    pub fn push(&mut self, undo_command: BoxUndoCommand) {
        self.commands.push_back(undo_command);
        // dbg!(&self.commands);
    }

    pub fn push_front(&mut self, undo_command: BoxUndoCommand) {
        self.commands.push_front(undo_command);
        // dbg!(&self.commands);
    }

    pub fn push_front_vec(&mut self, mut undo_commands: Vec<BoxUndoCommand>) {
        while let Some(undo_command) = undo_commands.pop() {
            self.push_front(undo_command);
        }
    }

    fn apply_commands(&mut self, world: &mut World) {
        // if !self.commands.is_empty() {
        //     dbg!(&self.commands);
        //     dbg!(&self.undo_stack);
        //     dbg!(&self.redo_stack);
        // }

        while let Some(command) = self.commands.pop_front() {
            command.forward(world, self);

            if command.command_type() == UndoCommandType::Command {
                self.command_batch.push_back(command);
                self.redo_stack.clear();
            }
        }
    }

    pub fn undo_stack(&self) -> &Vec<BoxUndoCommand> {
        &self.undo_stack
    }

    pub fn redo_stack(&self) -> &Vec<BoxUndoCommand> {
        &self.redo_stack
    }
}

pub struct UndoCommandManagerPlugin;
impl Plugin for UndoCommandManagerPlugin {
    fn build(&self, app: &mut App) {
        app.insert_non_send_resource(UndoCommandManager::new())
            .add_system_to_stage(
                PostUndoCommandStage,
                apply_commands.exclusive_system().at_start(),
            );
    }
}

fn apply_commands(world: &mut World) {
    let mut undo_command_manager = world
        .remove_resource::<UndoCommandManager>()
        .expect("we have no UndoCommandManager, something is very wrong");
    undo_command_manager.apply_commands(world);
    world.insert_resource(undo_command_manager);

    let mut engine = world
        .remove_resource::<Engine>()
        .expect("we have no engine, something is seriously wrong");
    engine.prepare();

    // Record dirtied nodes for thumbnail processing.
    let dag_id = *world.resource::<DagId>();
    let dirty_nodes = engine.nodes_in_state(dag_id, NodeState::Dirty);
    let mut q_nodes = world.query::<(Entity, &NodeAddressComponent)>();

    let q_nodes = q_nodes
        .iter(world)
        .filter(|(_, node_address)| node_address.dag_id() == dag_id)
        .map(|(entity, node_address)| (entity, node_address.node_id()))
        .collect::<Vec<_>>();

    for (entity, node_id) in q_nodes.iter() {
        if dirty_nodes.contains(node_id) {
            let is_dirtied = world.entity(*entity).contains::<Dirtied>();
            if !is_dirtied {
                world.entity_mut(*entity).insert(Dirtied);
            }
        } else {
            world.entity_mut(*entity).remove::<Dirtied>();
        }
    }

    // Send Engine for running in another thread.
    let sender = world
        .remove_non_send_resource::<mpsc::Sender<EngineThreadMessage>>()
        .expect("the sender should always exist");
    sender.send(EngineThreadMessage::Engine(engine)).unwrap();
    world.insert_non_send_resource(sender);
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Checkpoint;
impl UndoCommand for Checkpoint {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Checkpoint
    }

    fn forward(&self, _: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut command_vec: Vec<BoxUndoCommand> = Vec::new();

        while let Some(command) = undo_command_manager.command_batch.pop_front() {
            if command.command_type() != UndoCommandType::Command {
                undo_command_manager.command_batch.push_front(command);
                break;
            }
            command_vec.push(command);
        }

        match command_vec.len() {
            1 => undo_command_manager
                .undo_stack
                .push(command_vec.pop().unwrap()),
            2.. => undo_command_manager.undo_stack.push(Box::new(command_vec)),
            _ => (),
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("a `Checkpoint` is never put on the `undo_stack`")
    }
}

impl UndoCommand for Vec<BoxUndoCommand> {
    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        for command in self.iter() {
            assert_eq!(command.command_type(), UndoCommandType::Command);
            command.forward(world, undo_command_manager);
        }
    }

    fn backward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        for command in self.iter().rev() {
            assert_eq!(command.command_type(), UndoCommandType::Command);
            command.backward(world, undo_command_manager);
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct UndoCancel;
impl UndoCommand for UndoCancel {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        while let Some(command) = undo_command_manager.command_batch.pop_back() {
            command.backward(world, undo_command_manager);
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this `UndoCommand` is never put on the undo stack");
    }
}
