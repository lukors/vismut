// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use super::prelude::*;
use crate::{core_translation::Translator, properties_panel::SavedProperties};
use bevy::prelude::World;
use std::fmt::Debug;
use vismut_core::prelude::*;

#[derive(Debug)]
pub struct GuiUndoCommand<T, U>
where
    T: Debug + Clone,
    U: Translator<T>,
{
    contact_info: U,
    from: T,
    to: T,
}

impl<T: Debug + Clone, U: Translator<T>> UndoCommand for GuiUndoCommand<T, U> {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let mut saved_properties = world.resource_mut::<SavedProperties>();
        saved_properties.changed = true;

        let mut engine = world.resource_mut::<Engine>();
        self.contact_info.set(&mut engine, self.to.clone()).unwrap();
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let mut saved_properties = world.resource_mut::<SavedProperties>();
        saved_properties.changed = true;

        let mut engine = world.resource_mut::<Engine>();
        let _ = self.contact_info.set(&mut engine, self.from.clone());
    }
}

impl<T, U> GuiUndoCommand<T, U>
where
    T: Debug + Clone,
    U: Translator<T>,
{
    pub fn new(contact_info: U, from: T, to: T) -> Self {
        Self {
            contact_info,
            from,
            to,
        }
    }
}
