// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;
use vismut_core::address::NodeAddress;
use vismut_core::prelude::NodeType;

use crate::{
    mouse_interaction::{active::MakeNothingActive, select::DeselectItem, Selection},
    sync_graph::{self, GuiEdge},
};

use super::{edge::RemoveGuiEdgeOnly, prelude::*, undo_command_manager::BoxUndoCommand};

#[derive(Clone, Debug)]
pub struct AddNode {
    pub node_address: NodeAddress,
    pub node_type: NodeType,
    pub translation: Vec2,
}
impl UndoCommand for AddNode {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        {
            let mut engine = world.resource_mut::<vismut_core::prelude::Engine>();
            engine
                .insert_node_with_address(self.node_type.clone(), self.node_address)
                .expect("could not insert a node");
        }
        sync_graph::spawn_gui_node(world, self.node_address, self.translation);
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        sync_graph::remove_gui_node(world, self.node_address);
    }
}
impl AddNode {
    pub fn new(node_address: NodeAddress, node_type: NodeType, translation: Vec2) -> Self {
        Self {
            node_address,
            node_type,
            translation,
        }
    }
}

/// Removes only the `Node`, and none of the connected `Edge`s. You should almost always use
/// `RemoveNode` instead, which removes connected edges too.
#[derive(Clone, Debug)]
pub struct RemoveNodeOnly {
    pub node_address: NodeAddress,
    pub node_type: NodeType,
    pub translation: Vec2,
}
impl UndoCommand for RemoveNodeOnly {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        sync_graph::remove_gui_node(world, self.node_address);
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let mut engine = world.resource_mut::<vismut_core::prelude::Engine>();
        engine
            .insert_node_with_address(self.node_type.clone(), self.node_address)
            .expect("could not insert a node");
        sync_graph::spawn_gui_node(world, self.node_address, self.translation);
    }
}
impl RemoveNodeOnly {
    pub fn new(node_address: NodeAddress, node_type: NodeType, translation: Vec2) -> Self {
        Self {
            node_address,
            node_type,
            translation,
        }
    }
}

/// Removes the `Node` and all connected `Edge`s.
#[derive(Clone, Debug)]
pub struct RemoveNode {
    pub node_address: NodeAddress,
    pub node_type: NodeType,
    pub translation: Vec2,
}
impl UndoCommand for RemoveNode {
    fn command_type(&self) -> super::UndoCommandType {
        super::UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut commands: Vec<BoxUndoCommand> = Vec::new();

        let mut q_edge = world.query::<&GuiEdge>();

        for edge in q_edge.iter(world).filter(|edge| {
            edge.input_slot_address.without_slot_id() == self.node_address
                || edge.output_slot_address.without_slot_id() == self.node_address
        }) {
            commands.push(Box::new(RemoveGuiEdgeOnly(*edge)));
        }

        commands.push(Box::new(DeselectItem(Selection::Node(self.node_address))));
        commands.push(Box::new(MakeNothingActive));
        commands.push(Box::new(RemoveNodeOnly {
            node_address: self.node_address,
            node_type: self.node_type.clone(),
            translation: self.translation,
        }));

        undo_command_manager.push_front_vec(commands);
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is never put on the undo stack, so this can not be reached");
    }
}
impl RemoveNode {
    pub fn new(node_address: NodeAddress, node_type: NodeType, translation: Vec2) -> Self {
        Self {
            node_address,
            node_type,
            translation,
        }
    }
}
