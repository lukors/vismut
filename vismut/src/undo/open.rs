use std::path::PathBuf;

use crate::shared::SnapToGrid;
use crate::status_bar::{StatusText, Urgency};
use crate::{clear_graph, spawn_gui_node, GridPosition};
use bevy::prelude::*;
use vismut_core::prelude::*;

use super::{edge::add_gui_edge, prelude::*};

#[derive(Debug)]
pub struct OpenGraph(pub PathBuf);

impl UndoCommand for OpenGraph {
    fn command_type(&self) -> crate::undo::UndoCommandType {
        crate::undo::UndoCommandType::Custom
    }

    fn forward(
        &self,
        world: &mut bevy::prelude::World,
        undo_command_manager: &mut UndoCommandManager,
    ) {
        let file = match std::fs::File::open(&self.0) {
            Ok(file) => file,
            Err(e) => {
                let message = format!("could not open file: {}", e);
                warn!("{}", message);
                world
                    .resource_mut::<StatusText>()
                    .set(Urgency::Error, message);
                return;
            }
        };

        let reader = std::io::BufReader::new(file);
        let serializable_dag: SerializeableDag = match serde_json::de::from_reader(reader) {
            Ok(dag) => dag,
            Err(e) => {
                let message = format!("could not interpret file: {}", e);
                warn!("{}", message);
                world
                    .resource_mut::<StatusText>()
                    .set(Urgency::Error, message);
                return;
            }
        };

        undo_command_manager.clear();
        clear_graph(world);

        let new_dag_id = world
            .resource_mut::<Engine>()
            .insert(serializable_dag.dag.clone());
        *world.resource_mut::<DagId>() = new_dag_id;

        for node_id in serializable_dag.dag.node_id_iter() {
            let grid_position = serializable_dag
                .positions
                .iter()
                .find(|serialized_position| serialized_position.node_id == node_id)
                .map(|serialized_position| {
                    GridPosition::new(serialized_position.x, serialized_position.y)
                })
                .unwrap_or_default();

            let entity = spawn_gui_node(world, NodeAddress::new(new_dag_id, node_id), Vec2::ZERO);
            world
                .entity_mut(entity)
                .insert(SnapToGrid)
                .insert(grid_position);
        }

        for edge in world
            .resource::<Engine>()
            .dag_edges(new_dag_id)
            .expect("we just inserted this dag, so it should exist")
        {
            add_gui_edge(world, edge, None);
        }
    }

    fn backward(
        &self,
        _world: &mut bevy::prelude::World,
        _undo_command_manager: &mut UndoCommandManager,
    ) {
        unreachable!("this command is never put on the undo stack")
    }
}
