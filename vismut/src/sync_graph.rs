// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use std::num::NonZeroU8;
use std::{f32::consts::PI, fmt::Debug};

use crate::shared::{
    BACKPLATE_PADDING, FONT_SIZE, GRID_HEIGHT, GRID_WIDTH, NODE_HEIGHT, NODE_WIDTH,
    RGBA_SLOT_HEIGHT, RGBA_SLOT_WIDTH, SMALLEST_DEPTH_UNIT, THUMBNAIL_SIZE, TITLE_MARGIN_LEFT,
    TITLE_MARGIN_TOP,
};
use crate::{
    alpha_checkerboard::AlphaCheckeboardMaterial,
    hoverable::CustomHitSize,
    material::slot_type_to_color,
    shared::{NodeAddressComponent, NodeStateComponent, SlotTypeComponent},
    // thumbnail::{Thumbnail, ThumbnailState, THUMBNAIL_SIZE},
    Draggable,
    Hoverable,
    Hovered,
    FONT,
};
use bevy::sprite::Anchor;
use bevy::{
    prelude::*,
    sprite::{Material2dPlugin, MaterialMesh2dBundle, Mesh2dHandle},
    window::WindowResized,
};
use rand::Rng;
use vismut_core::prelude::*;

#[derive(Bundle, Default)]
pub struct BackplateBundle {
    #[bundle]
    sprite_bundle: SpriteBundle,
    grid_position: GridPosition,
    grid_size: GridSize,
}

#[derive(Component)]
pub struct Thumbnail;

pub trait Name {
    fn title(&self) -> String;
}

impl Name for NodeType {
    fn title(&self) -> String {
        self.to_string()
    }
}

#[derive(Component)]
pub struct NodeFrame;

// The start and end variables are for selecting the edges. Though they are currently unused.
#[derive(Component, Copy, Clone, Debug)]
pub struct GuiEdge {
    pub start: Vec2,
    pub end: Vec2,
    pub output_slot_address: SlotAddress,
    pub input_slot_address: SlotAddress,
}

impl PartialEq for GuiEdge {
    fn eq(&self, other: &Self) -> bool {
        self.output_slot_address == other.output_slot_address
            && self.input_slot_address == other.input_slot_address
    }
}

impl From<GuiEdge> for Edge {
    fn from(gui_edge: GuiEdge) -> Self {
        Self {
            slot_address_output: gui_edge.output_slot_address,
            slot_address_input: gui_edge.input_slot_address,
        }
    }
}

#[derive(Copy, Clone, Debug, Component)]
pub struct SlotFrame;

#[derive(Component, Deref, DerefMut, Copy, Clone, Debug, Default, PartialEq, Eq)]
pub(crate) struct SlotAddressSideComponent(pub SlotAddressSide);

/// Represents a position in the grid. Is used together with `GridSize` to ensure items don't
/// overlap in the grid.
#[derive(Component, Copy, Clone, Debug, PartialEq, Eq)]
pub struct GridPosition {
    pub x: i32,
    pub y: i32,
}

impl Default for GridPosition {
    fn default() -> Self {
        Self::new(0, 0)
    }
}

impl GridPosition {
    pub const fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }
}

/// A bundle for a GUI node.
#[derive(Bundle, Default)]
pub(crate) struct GuiNodeBundle {
    #[bundle]
    backplate_bundle: BackplateBundle,
    hoverable: Hoverable,
    hovered: Hovered,
    draggable: Draggable,
    node_address: NodeAddressComponent,
    node_state: NodeStateComponent,
}

/// Represents a size in the grid. Is used together with `GridPosition` to ensure items don't
/// overlap in the grid, and to render a "backplate" for items in the grid.
#[derive(Component)]
pub struct GridSize {
    pub width: NonZeroU8,
    pub height: NonZeroU8,
}

impl Default for GridSize {
    fn default() -> Self {
        Self::new()
    }
}

impl GridSize {
    fn new() -> Self {
        Self {
            width: NonZeroU8::new(1).unwrap(),
            height: NonZeroU8::new(1).unwrap(),
        }
    }
}

#[derive(Bundle, Default)]
pub(crate) struct SlotBundle {
    #[bundle]
    sprite_bundle: SpriteBundle,
    hoverable: Hoverable,
    draggable: Draggable,
    slot_address_side: SlotAddressSideComponent,
    slot_type: SlotTypeComponent,
}

pub(crate) struct SyncGraphPlugin;

impl Plugin for SyncGraphPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(Material2dPlugin::<AlphaCheckeboardMaterial>::default())
            .add_startup_system(setup)
            .add_system(relayout_text);
    }
}

// text is laid out once when created and when it changed. by default, moving a window to another screen won't
// re-layout. this means text is created once with the scale factor of the screen where the window was initially
// and it keeps that scale when moving the window to another screen with different dpi.
// this system forces a relayout of the text when the window is moved to another screen
// bug report and PR fixing it in >0.7.0 : https://github.com/bevyengine/bevy/pull/4689
fn relayout_text(
    mut window_resized_events: EventReader<WindowResized>,
    mut q_text: Query<&mut Text>,
) {
    if window_resized_events.iter().count() > 0 {
        for mut text in q_text.iter_mut() {
            let _iter = text.sections.iter_mut();
        }
    }
}

fn setup(mut commands: Commands, mut engine: ResMut<Engine>) {
    let dag = Dag::new();
    let dag_id = engine.insert(dag);
    commands.insert_resource(dag_id);
}

pub(crate) fn stretch_between(
    sprite: &mut Sprite,
    transform: &mut Transform,
    start: Vec2,
    end: Vec2,
) {
    let midpoint = (start + end) / 2.;
    let distance = start.distance(end);
    let rotation = Vec2::X.angle_between(start - end);

    transform.translation = midpoint.extend(9.0);
    transform.rotation = Quat::from_rotation_z(rotation);
    sprite.custom_size = Some(Vec2::new(distance, 4.0));
}

/// Removes the active graph from the `Engine`,
/// and removes all the corresponding nodes and edges in the GUI.
///
/// If you don't put another DAG into the `Engine` and update the `DagId` resource,
/// the program will probably crash.
pub fn clear_graph(world: &mut World) {
    let dag_id = *world.resource::<DagId>();
    let _ = world.resource_mut::<Engine>().remove(dag_id);

    let nodes = world
        .query_filtered::<Entity, Or<(With<NodeAddressComponent>, With<GuiEdge>)>>()
        .iter(world)
        .collect::<Vec<Entity>>();

    for entity in nodes {
        despawn_with_children_recursive(world, entity);
    }
}

pub fn remove_gui_node(world: &mut World, node_address: NodeAddress) {
    // TODO split from gui
    world
        .resource_mut::<Engine>()
        .remove_node(node_address)
        .unwrap();
    let (entity, _) = world
        .query::<(Entity, &NodeAddressComponent)>()
        .iter(world)
        .find(|(_, node_id_cmp)| node_address == node_id_cmp.0)
        .unwrap();
    despawn_with_children_recursive(world, entity);
}

pub fn spawn_gui_node(world: &mut World, node_address: NodeAddress, translation: Vec2) -> Entity {
    let node_type = {
        let engine = world.resource::<Engine>();
        engine.node_type(node_address).expect("should find node")
    }
    .clone();

    let mut meshes = world.remove_resource::<Assets<Mesh>>().unwrap();
    let mut custom_materials = world
        .remove_resource::<Assets<AlphaCheckeboardMaterial>>()
        .unwrap();

    let font = world.resource::<AssetServer>().load(FONT);

    let mesh = meshes.add(Mesh::from(shape::Quad {
        size: Vec2::new(THUMBNAIL_SIZE as f32, THUMBNAIL_SIZE as f32),
        flip: false,
    }));

    let scale = world
        .resource::<Windows>()
        .get_primary()
        .unwrap()
        .scale_factor() as f32;

    let entity = world
        .spawn()
        .insert_bundle(GuiNodeBundle {
            // Base
            backplate_bundle: BackplateBundle {
                sprite_bundle: SpriteBundle {
                    sprite: Sprite {
                        color: Color::rgb(0.18, 0.18, 0.18),
                        custom_size: Some(Vec2::new(NODE_WIDTH, NODE_HEIGHT)),
                        anchor: Anchor::TopLeft,
                        ..default()
                    },
                    transform: Transform::from_translation(Vec3::new(
                        translation.x,
                        translation.y,
                        rand::thread_rng().gen_range(0.0..9.0),
                    )),
                    ..default()
                },
                grid_size: GridSize {
                    width: NonZeroU8::new(6).unwrap(),
                    height: NonZeroU8::new(5).unwrap(),
                },
                ..default()
            },
            node_address: NodeAddressComponent(node_address),
            ..default()
        })
        .with_children(|parent| {
            // Title
            parent.spawn_bundle(Text2dBundle {
                text: Text::with_section(
                    node_type.title(),
                    TextStyle {
                        font,
                        font_size: FONT_SIZE as f32,
                        color: Color::WHITE,
                    },
                    TextAlignment {
                        horizontal: HorizontalAlign::Left,
                        vertical: VerticalAlign::Top,
                    },
                ),
                transform: Transform::from_translation(Vec3::new(
                    TITLE_MARGIN_LEFT,
                    -TITLE_MARGIN_TOP,
                    SMALLEST_DEPTH_UNIT,
                )),
                ..default()
            });

            // Thumbnail
            parent
                .spawn_bundle(SpriteBundle {
                    sprite: Sprite {
                        custom_size: Some(Vec2::new(THUMBNAIL_SIZE as f32, THUMBNAIL_SIZE as f32)),
                        anchor: Anchor::TopLeft,
                        ..default()
                    },
                    transform: Transform::from_translation(Vec3::new(
                        GRID_WIDTH as f32,
                        -(GRID_HEIGHT as f32) + BACKPLATE_PADDING as f32 * 2.0,
                        SMALLEST_DEPTH_UNIT * 2.0,
                    )),
                    ..default()
                })
                .insert(Thumbnail)
                .with_children(|parent| {
                    // Checkerboard
                    parent.spawn_bundle(MaterialMesh2dBundle {
                        mesh: Mesh2dHandle(mesh),
                        material: custom_materials.add(AlphaCheckeboardMaterial { scale }),
                        transform: Transform::from_translation(Vec3::new(
                            THUMBNAIL_SIZE as f32 / 2.0,
                            -(THUMBNAIL_SIZE as f32) / 2.0,
                            -SMALLEST_DEPTH_UNIT,
                        )),
                        ..default()
                    });
                });

            // Slots
            for (i, slot) in node_type.slots(Side::Input).into_iter().enumerate() {
                create_slot(parent, false, i, slot, node_address);
            }

            for (i, slot) in node_type.slots(Side::Output).into_iter().enumerate() {
                create_slot(parent, true, i, slot, node_address);
            }

            // Frame
            parent
                .spawn_bundle(SpriteBundle {
                    sprite: Sprite {
                        custom_size: Some(Vec2::new(NODE_WIDTH, NODE_HEIGHT)),
                        anchor: Anchor::TopLeft,
                        ..default()
                    },
                    transform: Transform::from_translation(-Vec3::new(
                        BACKPLATE_PADDING as f32,
                        -(BACKPLATE_PADDING as f32),
                        SMALLEST_DEPTH_UNIT,
                    )),
                    ..default()
                })
                .insert(NodeFrame);
        })
        .id();

    world.insert_resource(meshes);
    world.insert_resource(custom_materials);

    entity
}

fn create_slot(
    parent: &mut WorldChildBuilder,
    output: bool,
    i: usize,
    slot: Slot,
    node_address: NodeAddress,
) {
    let (side, mut pos_x) = if output {
        (Side::Output, GRID_WIDTH as f32 * 5.0)
    } else {
        (Side::Input, 0.0)
    };
    pos_x += GRID_WIDTH as f32 / 2.0;

    let pos_y = -((GRID_HEIGHT * (i + 1)) as f32) - GRID_HEIGHT as f32 / 2.0;

    let translation = Vec3::new(pos_x, pos_y, SMALLEST_DEPTH_UNIT * 2.0);

    let (rotation, custom_size) = if let SlotType::Gray = slot.slot_type {
        (
            Quat::from_rotation_z(PI * 0.25),
            Vec2::new(RGBA_SLOT_WIDTH, RGBA_SLOT_WIDTH) + Vec2::splat(2.0),
        )
    } else {
        (
            Quat::default(),
            Vec2::new(RGBA_SLOT_WIDTH, RGBA_SLOT_HEIGHT),
        )
    };

    let color = slot_type_to_color(slot.slot_type, 0.6);

    let hit_size = Vec2::new(GRID_WIDTH as f32, GRID_HEIGHT as f32);

    parent
        .spawn_bundle(SlotBundle {
            sprite_bundle: SpriteBundle {
                sprite: Sprite {
                    color,
                    custom_size: Some(custom_size - Vec2::splat(2.0)),
                    ..default()
                },
                transform: Transform {
                    rotation,
                    translation,
                    ..default()
                },
                ..default()
            },
            slot_address_side: SlotAddressSideComponent(
                node_address.with_slot_id(slot.slot_id).with_side(side),
            ),
            slot_type: SlotTypeComponent(slot.slot_type),
            ..default()
        })
        .insert(CustomHitSize(hit_size))
        .with_children(|parent| {
            parent
                .spawn_bundle(SpriteBundle {
                    sprite: Sprite {
                        color: Color::BLACK,
                        custom_size: Some(custom_size),
                        ..default()
                    },
                    transform: Transform::from_translation(-Vec3::Z * SMALLEST_DEPTH_UNIT),
                    ..default()
                })
                .insert(SlotFrame);
        });
}
