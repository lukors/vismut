// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;
use bevy_egui::{
    egui::{self, Align2, Color32, RichText},
    EguiContext,
};

use crate::CustomStage;

#[derive(Copy, Clone)]
pub enum Urgency {
    None,
    Warn,
    Error,
}

impl Urgency {
    fn text_color(self) -> Color32 {
        match self {
            Self::None => Color32::WHITE,
            Self::Warn => Color32::BLACK,
            Self::Error => Color32::WHITE,
        }
    }

    fn backgorund_color(self) -> Color32 {
        match self {
            Self::None => Color32::BLACK,
            Self::Warn => Color32::from_rgb(246, 196, 0),
            Self::Error => Color32::from_rgb(200, 76, 76),
        }
    }
}

pub struct StatusText {
    text: String,
    urgency: Urgency,
}

impl StatusText {
    pub fn set(&mut self, urgency: Urgency, text: String) {
        self.text = text;
        self.urgency = urgency;
    }
}

impl Default for StatusText {
    fn default() -> Self {
        const VERSION: &str = env!("CARGO_PKG_VERSION");
        Self {
            text: format!("Welcome to Vismut v{}!", VERSION),
            urgency: Urgency::None,
        }
    }
}

pub(crate) struct StatusBarPlugin;

impl Plugin for StatusBarPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(StatusText::default())
            .add_system_set_to_stage(
                CoreStage::Update,
                SystemSet::new()
                    .after(CustomStage::Setup)
                    .with_system(status_bar),
            );
    }
}

fn status_bar(mut egui_context: ResMut<EguiContext>, status_text: ResMut<StatusText>) {
    egui::Area::new("status_bar")
        .anchor(Align2::LEFT_BOTTOM, (0.0, 0.0))
        .movable(false)
        .show(egui_context.ctx_mut(), |ui| {
            ui.label(
                RichText::new(status_text.text.clone())
                    .background_color(status_text.urgency.backgorund_color())
                    .color(status_text.urgency.text_color()),
            );
        });
}
