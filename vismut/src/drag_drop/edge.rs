// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::{anyhow, Result};
use std::fmt::Debug;

use crate::{
    control_pressed,
    hoverable::{box_contains_point, CustomHitSize},
    material::{slot_type_to_color, HUE_AQUA},
    mouse_interaction::select::Selected,
    scan_code_input::ScanCodeInput,
    shared::{GRID_HEIGHT, GRID_WIDTH, SMALLEST_DEPTH_UNIT},
    stretch_between,
    sync_graph::GuiEdge,
    undo::{
        edge::{AddEdge, RemoveGuiEdge},
        prelude::*,
        undo_command_manager::Checkpoint,
    },
    Cursor, SlotAddressSideComponent, ToolState,
};
use bevy::prelude::*;
use vismut_core::address::Side;
use vismut_core::prelude::*;

use super::Dragged;

#[derive(Component, Deref, DerefMut, Copy, Clone, Debug)]
pub struct SourceSlot(SlotAddressSide);

#[derive(Component)]
pub struct GrabbedEdge {
    start: Vec2,
    slot: SlotAddressSide,
}

#[derive(Component)]
pub struct EdgeColor;

#[derive(Component)]
pub struct SlotFrame;

#[derive(Component)]
pub struct SlotFrameSprite;

pub(super) fn setup(mut commands: Commands) {
    commands
        .spawn()
        .insert(SlotFrame)
        .insert(Visibility { is_visible: false })
        .insert(Transform::from_translation(Vec3::Z * 10.0))
        .insert(GlobalTransform::default())
        .with_children(|parent| {
            spawn_frame_outline(parent);
        });
}

fn spawn_frame_outline(child_builder: &mut ChildBuilder) {
    const THICKNESS: f32 = 3.0;

    fn frame_side(child_builder: &mut ChildBuilder, translation: Vec2, size: Vec2) {
        child_builder
            .spawn_bundle(SpriteBundle {
                sprite: Sprite {
                    color: Color::hsl(HUE_AQUA, 0.5, 0.6),
                    custom_size: Some(size),
                    ..default()
                },
                transform: Transform::from_translation(translation.extend(0.0)),
                visibility: Visibility { is_visible: false },
                ..default()
            })
            .insert(SlotFrameSprite);
    }

    let size = Vec2::new(GRID_WIDTH as f32, GRID_HEIGHT as f32);

    frame_side(
        child_builder,
        Vec2::X * (size.x / 2.0),
        Vec2::new(THICKNESS, size.y + THICKNESS),
    );
    frame_side(
        child_builder,
        -Vec2::X * (size.x / 2.0),
        Vec2::new(THICKNESS, size.y + THICKNESS),
    );
    frame_side(
        child_builder,
        Vec2::Y * (size.y / 2.0),
        Vec2::new(size.x + THICKNESS, THICKNESS),
    );
    frame_side(
        child_builder,
        -Vec2::Y * (size.y / 2.0),
        Vec2::new(size.x + THICKNESS, THICKNESS),
    );
}

/// Grab all selected slots.
pub(crate) fn grab_tool_slot_setup(
    mut commands: Commands,
    mut tool_state: ResMut<State<ToolState>>,
    q_selected_slots: Query<(Entity, &GlobalTransform, &SlotAddressSideComponent), With<Selected>>,
    q_slot: Query<(&GlobalTransform, &SlotAddressSideComponent)>,
    mut q_edge: Query<(&mut Visibility, &GuiEdge)>,
    scan_code_input: Res<ScanCodeInput>,
    engine: Res<Engine>,
) {
    if q_selected_slots.iter().next().is_none() {
        tool_state.overwrite_replace(ToolState::None).unwrap();
    }

    for (entity, global_transform, slot_address_side) in q_selected_slots.iter() {
        commands.entity(entity).insert(Dragged {
            start: global_transform.translation.truncate(),
        });

        if control_pressed(&scan_code_input) {
            match slot_address_side.side {
                Side::Output => {
                    for (mut visibility, edge) in q_edge.iter_mut().filter(|(_, edge)| {
                        edge.output_slot_address == slot_address_side.without_side()
                    }) {
                        visibility.is_visible = false;

                        if let Some((input_slot_gtransform, input_slot)) =
                            q_slot.iter().find(|(_, slot)| {
                                slot.0 == edge.input_slot_address.with_side(Side::Input)
                            })
                        {
                            spawn_grabbed_edge(
                                &mut commands,
                                &*engine,
                                Some(SourceSlot(**slot_address_side)),
                                input_slot_gtransform.translation.truncate(),
                                input_slot.0,
                            );
                        }
                    }
                }
                Side::Input => {
                    if let Some((mut visibility, edge)) = q_edge.iter_mut().find(|(_, edge)| {
                        edge.input_slot_address == slot_address_side.without_side()
                    }) {
                        visibility.is_visible = false;

                        if let Some((output_slot_gtransform, output_slot)) =
                            q_slot.iter().find(|(_, slot)| {
                                slot.0 == edge.output_slot_address.with_side(Side::Output)
                            })
                        {
                            spawn_grabbed_edge(
                                &mut commands,
                                &*engine,
                                Some(SourceSlot(**slot_address_side)),
                                output_slot_gtransform.translation.truncate(),
                                output_slot.0,
                            );
                        }
                    }
                }
            }
        } else {
            spawn_grabbed_edge(
                &mut commands,
                &*engine,
                None,
                global_transform.translation.truncate(),
                **slot_address_side,
            );
        }
    }
}

/// When an edge is dropped, this system updates the node graph based on where its dropped, and
/// removes the edges.
#[allow(clippy::too_many_arguments)]
pub(crate) fn grab_edge_update(
    mut undo_command_manager: ResMut<UndoCommandManager>,
    mut tool_state: ResMut<State<ToolState>>,
    mut i_mouse_button: ResMut<Input<MouseButton>>,
    q_slot: Query<(&GlobalTransform, &SlotAddressSideComponent, &CustomHitSize)>,
    q_cursor: Query<&GlobalTransform, With<Cursor>>,
    mut q_grabbed_edge: Query<
        (
            &mut Transform,
            &mut Sprite,
            &GrabbedEdge,
            Option<&SourceSlot>,
        ),
        Without<SlotAddressSideComponent>,
    >,
    q_edge: Query<&GuiEdge, Without<GrabbedEdge>>,
    mut q_slot_frame: Query<
        (&mut Visibility, &mut Transform),
        (With<SlotFrame>, Without<GrabbedEdge>),
    >,
) {
    let cursor_t = q_cursor.iter().next().unwrap();

    if i_mouse_button.just_released(MouseButton::Left) {
        i_mouse_button.clear();

        let mut new_edges = Vec::new();

        'outer: for (_, _, grabbed_edge, source_slot) in q_grabbed_edge.iter() {
            for (slot_t, slot, custom_hit_size) in q_slot.iter() {
                if box_contains_point(
                    slot_t.translation.truncate(),
                    **custom_hit_size,
                    cursor_t.translation.truncate(),
                ) {
                    if let Some(source_slot) = source_slot {
                        if **source_slot != **slot {
                            if let Ok(add_edge) = connect_arbitrary(**slot, grabbed_edge.slot) {
                                new_edges.push(Box::new(add_edge));
                            }
                            for edge in q_edge.iter() {
                                if (edge.input_slot_address == source_slot.without_side()
                                    && edge.output_slot_address == grabbed_edge.slot.without_side())
                                    || (edge.output_slot_address == source_slot.without_side()
                                        && edge.input_slot_address
                                            == grabbed_edge.slot.without_side())
                                {
                                    undo_command_manager.push(Box::new(RemoveGuiEdge(*edge)));
                                }
                            }
                        }
                    } else if let Ok(add_edge) = connect_arbitrary(**slot, grabbed_edge.slot) {
                        new_edges.push(Box::new(add_edge));
                    }

                    continue 'outer;
                }
            }

            if let Some(source_slot) = source_slot {
                for edge in q_edge.iter() {
                    if (edge.input_slot_address == source_slot.without_side()
                        && edge.output_slot_address == grabbed_edge.slot.without_side())
                        || (edge.output_slot_address == source_slot.without_side()
                            && edge.input_slot_address == grabbed_edge.slot.without_side())
                    {
                        undo_command_manager.push(Box::new(RemoveGuiEdge(*edge)));
                    }
                }
            }
        }

        for new_edge in new_edges {
            undo_command_manager.push(new_edge);
        }
        undo_command_manager.push(Box::new(Checkpoint));
        tool_state.overwrite_replace(ToolState::None).unwrap();
    } else {
        // Update grabbed edge
        for (mut edge_t, mut sprite, edge, _) in q_grabbed_edge.iter_mut() {
            stretch_between(
                &mut sprite,
                &mut edge_t,
                edge.start,
                cursor_t.translation.truncate(),
            );
        }

        // Frame hovered slot
        if let (Ok((mut visibility, mut transform)), Some(grabbed_slot_side)) = (
            q_slot_frame.get_single_mut(),
            q_grabbed_edge
                .iter()
                .next()
                .map(|(_, _, grabbed_edge, _)| grabbed_edge.slot.side),
        ) {
            let mut hovered = false;

            for (slot_t, _slot, custom_hit_size) in q_slot
                .iter()
                .filter(|(_, slot, _)| slot.0.side != grabbed_slot_side)
            {
                if box_contains_point(
                    slot_t.translation.truncate(),
                    custom_hit_size.0,
                    cursor_t.translation.truncate(),
                ) {
                    hovered = true;
                    transform.translation.x = slot_t.translation.x;
                    transform.translation.y = slot_t.translation.y;
                    break;
                }
            }

            visibility.is_visible = hovered;
        }
    }
}

pub(super) fn propagate_frame_visibility(
    q_slot_frame: Query<&Visibility, (With<SlotFrame>, Changed<Visibility>)>,
    mut q_frame_sprites: Query<&mut Visibility, (With<SlotFrameSprite>, Without<SlotFrame>)>,
) {
    if let Ok(visibility) = q_slot_frame.get_single() {
        for mut visibility_sprite in q_frame_sprites.iter_mut() {
            visibility_sprite.is_visible = visibility.is_visible;
        }
    }
}

pub(super) fn update_edge_color(
    mut q_edge_color: Query<(&Parent, &mut Sprite, &mut Visibility), With<EdgeColor>>,
    q_edge_parent: Query<
        (Entity, &Sprite, &Visibility),
        (
            Or<(Changed<GuiEdge>, Changed<Visibility>, With<GrabbedEdge>)>,
            Without<EdgeColor>,
        ),
    >,
) {
    for (parent, mut color_sprite, mut visibility) in q_edge_color.iter_mut() {
        if let Some((_, parent_sprite, parent_visibility)) = q_edge_parent
            .iter()
            .find(|(entity, ..)| *entity == parent.0)
        {
            let length = parent_sprite.custom_size.unwrap().x;
            color_sprite.custom_size = Some(Vec2::new(length, 2.0));

            visibility.is_visible = parent_visibility.is_visible;
        }
    }
}

fn spawn_grabbed_edge(
    commands: &mut Commands,
    engine: &Engine,
    source_slot: Option<SourceSlot>,
    start: Vec2,
    slot_address_side: SlotAddressSide,
) {
    let color = {
        let node_type = engine.node_type(slot_address_side.node_address()).unwrap();

        let slot = if slot_address_side.side == Side::Input {
            node_type
                .slot_from_id(Side::Input, slot_address_side.slot_id)
                .unwrap()
        } else {
            node_type
                .slot_from_id(Side::Output, slot_address_side.slot_id)
                .unwrap()
        };

        slot_type_to_color(slot.slot_type, 0.7)
    };

    let mut edge = commands.spawn_bundle(SpriteBundle {
        sprite: Sprite {
            color: Color::BLACK,
            custom_size: Some(Vec2::new(4.0, 4.0)),
            ..default()
        },
        ..default()
    });

    edge.insert(GrabbedEdge {
        start,
        slot: slot_address_side,
    })
    .with_children(|parent| {
        parent
            .spawn_bundle(SpriteBundle {
                sprite: Sprite {
                    color,
                    custom_size: Some(Vec2::new(2.0, 2.0)),
                    ..default()
                },
                transform: Transform::from_translation(Vec3::Z * SMALLEST_DEPTH_UNIT),
                ..default()
            })
            .insert(EdgeColor);
    });

    if let Some(source_slot) = source_slot {
        edge.insert(source_slot);
    }
}

fn connect_arbitrary(slot_a: SlotAddressSide, slot_b: SlotAddressSide) -> Result<AddEdge> {
    if let Ok(edge) = Edge::from_sided(slot_a, slot_b) {
        Ok(AddEdge(edge))
    } else {
        Err(anyhow!("could not connect slot"))
    }
}

/// Drops all grabbed entities.
pub(crate) fn grab_edge_cleanup(
    mut commands: Commands,
    q_grabbed_edge: Query<Entity, With<GrabbedEdge>>,
    q_dragged: Query<Entity, With<Dragged>>,
    mut q_edge: Query<&mut Visibility, With<GuiEdge>>,
    mut q_slot_frame: Query<&mut Visibility, (With<SlotFrame>, Without<GuiEdge>)>,
) {
    for entity in q_grabbed_edge.iter() {
        commands.entity(entity).despawn_recursive();
    }

    for entity in q_dragged.iter() {
        commands.entity(entity).remove::<Dragged>();
        commands.entity(entity).remove::<Selected>();
    }

    for mut visibility in q_edge.iter_mut() {
        *visibility = Visibility { is_visible: true };
    }

    if let Ok(mut visibility) = q_slot_frame.get_single_mut() {
        visibility.is_visible = false;
    }
}
