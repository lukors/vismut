// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use std::fmt::Debug;

use anyhow::{anyhow, Result};
use vismut_core::prelude::*;

pub trait Translator<DataType>: Debug {
    fn get(&self, engine: &Engine) -> Result<DataType>;
    fn set(&self, engine: &mut Engine, value: DataType) -> Result<()>;
}

impl Translator<NodeType> for NodeAddress {
    fn get(&self, engine: &Engine) -> Result<NodeType> {
        let node_type = engine.node_type(*self)?;
        Ok(node_type.clone())
    }

    fn set(&self, engine: &mut Engine, value: NodeType) -> Result<()> {
        *engine.node_type_mut(*self)? = value;
        Ok(())
    }
}

impl Translator<Resize> for NodeAddress {
    fn get(&self, engine: &Engine) -> Result<Resize> {
        let node_type = engine.node_type(*self)?;
        node_type
            .resize()
            .ok_or_else(|| anyhow!("NodeType does not have a resize: {}", node_type))
    }

    fn set(&self, engine: &mut Engine, value: Resize) -> Result<()> {
        let node_type = engine.node_type_mut(*self)?;
        let resize = node_type
            .resize_mut()
            .ok_or_else(|| anyhow!("NodeType does not have a resize"))?;

        *resize = value;

        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct TranslateGrayscaleValue(pub VismutPixel);
impl Translator<TranslateGrayscaleValue> for NodeAddress {
    fn get(&self, engine: &Engine) -> Result<TranslateGrayscaleValue> {
        let node_type = engine.node_type(*self)?;

        if let NodeType::Grayscale(GrayscaleNode(val)) = *node_type {
            Ok(TranslateGrayscaleValue(val))
        } else {
            Err(anyhow!("not a `NodeType::Grayscale: {}", node_type))
        }
    }

    fn set(&self, engine: &mut Engine, value: TranslateGrayscaleValue) -> Result<()> {
        let node_type = engine.node_type_mut(*self)?;
        if let NodeType::Grayscale(ref mut value_mut) = node_type {
            value_mut.0 = value.0;
        }
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct TranslateName(pub String);
impl Translator<TranslateName> for NodeAddress {
    fn get(&self, engine: &Engine) -> Result<TranslateName> {
        let name = engine.node_type(*self)?.name()?;
        Ok(TranslateName(name.clone()))
    }

    fn set(&self, engine: &mut Engine, name: TranslateName) -> Result<()> {
        engine
            .set_name(*self, name.0)
            .map_err(|_| anyhow!("wrong node type"))
    }
}
