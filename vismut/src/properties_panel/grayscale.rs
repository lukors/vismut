// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::core_translation::TranslateGrayscaleValue;
use crate::undo::prelude::*;
use bevy_egui::egui;
use bevy_egui::egui::Ui;
use vismut_core::prelude::*;

pub fn display(
    ui: &mut Ui,
    undo_command_manager: &mut UndoCommandManager,
    node_address: NodeAddress,
    node_type: &NodeType,
) {
    let value_current = if let NodeType::Grayscale(GrayscaleNode(value)) = *node_type {
        value
    } else {
        return;
    };

    ui.label("Value");
    let mut value_new = value_current;
    let response = ui.add(egui::Slider::new(&mut value_new, 0.0..=1.0));

    if (response.drag_released() || response.lost_focus()) && value_new != value_current {
        let grayscale_current = TranslateGrayscaleValue(value_current);
        let grayscale_new = TranslateGrayscaleValue(value_new);

        undo_command_manager.push(Box::new(GuiUndoCommand::new(
            node_address,
            grayscale_current,
            grayscale_new,
        )));
        undo_command_manager.push(Box::new(Checkpoint));
    }

    ui.end_row();
}
