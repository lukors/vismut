// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;
use vismut_core::prelude::*;

// Grid
pub const GRID_WIDTH: usize = THUMBNAIL_SIZE / 4;
pub const GRID_HEIGHT: usize = THUMBNAIL_SIZE / 4;
/// The visual size of the points in the background grid.
pub const GRID_POINT_SIZE: usize = 3;
/// The number of pixels between a backplate and its outer grid border. If two backplates go up
/// against each other they have this times two pixels of distance between them.
pub const BACKPLATE_PADDING: usize = 2;

// Node
pub const FONT_SIZE: usize = 22;
pub const TITLE_MARGIN_LEFT: f32 = (GRID_WIDTH - FONT_SIZE - BACKPLATE_PADDING) as f32;
pub const TITLE_MARGIN_TOP: f32 = TITLE_MARGIN_LEFT / 2.0;
pub const THUMBNAIL_SIZE: usize = 128;
// Size in grid units
pub const NODE_WIDTH_GRID: usize = 6;
pub const NODE_HEIGHT_GRID: usize = 5;
// Size in pixels
pub const NODE_WIDTH: f32 = NODE_WIDTH_GRID as f32 * GRID_WIDTH as f32;
pub const NODE_HEIGHT: f32 = NODE_HEIGHT_GRID as f32 * GRID_HEIGHT as f32;

// Slot
pub const RGBA_SLOT_WIDTH: f32 = 10.0;
pub const RGBA_SLOT_HEIGHT: f32 = 16.0;

pub const SMALLEST_DEPTH_UNIT: f32 = f32::EPSILON * 500.;

#[derive(Component, Deref, DerefMut, Default)]
pub struct NodeAddressComponent(pub NodeAddress);

#[derive(Component, Deref, DerefMut, Default)]
pub struct NodeStateComponent(pub NodeState);

#[derive(Component, Deref, DerefMut, Default)]
pub struct SlotTypeComponent(pub SlotType);

#[derive(Component, Default)]
pub struct SnapToGrid;
