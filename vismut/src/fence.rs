// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;

use crate::shared::{GRID_HEIGHT, GRID_WIDTH, NODE_HEIGHT, NODE_WIDTH};
use crate::{camera::WorkspaceCamera, shared::NodeAddressComponent};

/// Ensures the camera never leaves the node graph.
pub struct FencePlugin;
impl Plugin for FencePlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set_to_stage(CoreStage::Update, SystemSet::new().with_system(fence));
    }
}

fn fence(
    windows: Res<Windows>,
    q_nodes: Query<&Transform, With<NodeAddressComponent>>,
    mut q_camera: Query<
        &mut Transform,
        (
            With<WorkspaceCamera>,
            Without<NodeAddressComponent>,
            Changed<Transform>,
        ),
    >,
) {
    let window = windows.get_primary().unwrap();

    let node_bounds = q_nodes
        .iter()
        .map(|transform| {
            (
                transform.translation.truncate(),
                transform.translation.truncate(),
            )
        })
        .reduce(|acc, next| (Vec2::min(acc.0, next.0), Vec2::max(acc.1, next.1)));

    if let Some(node_bounds) = node_bounds {
        if node_bounds == (Vec2::new(0.0, 0.0), Vec2::new(0.0, 0.0)) {
            return;
        }

        let half_window_size = Vec2::new(window.width() / 2.0, window.height() / 2.0);
        let node_size = Vec2::new(NODE_WIDTH, NODE_HEIGHT);

        let min = node_bounds.0 - half_window_size - node_size - GRID_WIDTH as f32 / 2.0;
        let max = node_bounds.1 + half_window_size + node_size + GRID_HEIGHT as f32 / 2.0;

        if let Ok(mut camera_transform) = q_camera.get_single_mut() {
            while camera_transform.translation.x < min.x {
                camera_transform.translation.x += GRID_WIDTH as f32;
            }
            while camera_transform.translation.x > max.x {
                camera_transform.translation.x -= GRID_WIDTH as f32;
            }
            while camera_transform.translation.y < min.y {
                camera_transform.translation.y += GRID_HEIGHT as f32;
            }
            while camera_transform.translation.y > max.y {
                camera_transform.translation.y -= GRID_HEIGHT as f32;
            }
        }
    }
}
