// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

/// Box select tool
use bevy::prelude::*;

use crate::{workspace::Workspace, AmbiguitySet, CustomStage};

pub(crate) const CAMERA_DISTANCE: f32 = 10.;

#[derive(Component)]
pub(crate) struct WorkspaceCameraAnchor;

#[derive(Component)]
pub(crate) struct WorkspaceCamera;

#[derive(Debug, Hash, PartialEq, Eq, Clone, SystemLabel)]
pub(crate) enum FirstPersonState {
    Off,
    // On,
}

impl Default for FirstPersonState {
    fn default() -> Self {
        Self::Off
    }
}

#[derive(Component)]
pub(crate) struct Cursor;
#[derive(Component)]
pub(crate) struct Crosshair;

pub(crate) struct CameraPlugin;

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.add_state(FirstPersonState::Off)
            .add_startup_system(setup.in_ambiguity_set(AmbiguitySet))
            .add_system_set_to_stage(
                CoreStage::Update,
                SystemSet::new()
                    .label(CustomStage::Setup)
                    .after(CustomStage::Input)
                    .with_system(
                        first_person_off_update
                            .with_run_criteria(State::on_update(FirstPersonState::Off))
                            .in_ambiguity_set(AmbiguitySet),
                    )
                    .with_system(
                        mouse_pan
                            .with_run_criteria(State::on_update(FirstPersonState::Off))
                            .in_ambiguity_set(AmbiguitySet),
                    ),
            );
    }
}

fn setup(mut commands: Commands) {
    commands
        .spawn_bundle(OrthographicCameraBundle::new_2d())
        .insert(WorkspaceCamera)
        .with_children(|parent| {
            parent
                .spawn()
                .insert(Transform::from_translation(Vec3::new(
                    0.,
                    0.,
                    -CAMERA_DISTANCE,
                )))
                .insert(GlobalTransform::default())
                .insert(WorkspaceCameraAnchor);
        });
    commands
        .spawn()
        .insert(Transform::default())
        .insert(GlobalTransform::default())
        .insert(Cursor);
}

fn first_person_off_update(
    mut q_cursor: Query<&mut Transform, With<Cursor>>,
    workspace: Res<Workspace>,
) {
    for mut transform in q_cursor.iter_mut() {
        transform.translation.x = workspace.cursor_world.x;
        transform.translation.y = workspace.cursor_world.y;
    }
}

/// Pan using the mouse.
fn mouse_pan(
    workspace: Res<Workspace>,
    windows: Res<Windows>,
    mut camera: Query<&mut Transform, With<WorkspaceCamera>>,
    i_mouse_button: Res<Input<MouseButton>>,
) {
    if i_mouse_button.pressed(MouseButton::Middle) && workspace.cursor_moved {
        let window = windows.get_primary().unwrap();
        let scale = window.backend_scale_factor();

        if let Ok(mut camera_t) = camera.get_single_mut() {
            camera_t.translation.x -= workspace.cursor_delta.x * (1.0 / scale as f32);
            camera_t.translation.y += workspace.cursor_delta.y * (1.0 / scale as f32);
        }
    }
}
