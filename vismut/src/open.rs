// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::scan_code_input::ScanCodeInput;
use crate::{
    status_bar::{StatusText, Urgency},
    undo::{
        open::OpenGraph,
        prelude::{Checkpoint, UndoCommandManager},
    },
    AmbiguitySet, CustomStage, ToolState,
};
use bevy::prelude::*;
use native_dialog::FileDialog;

pub(crate) struct OpenPlugin;

impl Plugin for OpenPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set_to_stage(
            CoreStage::Update,
            SystemSet::new()
                .label(CustomStage::Apply)
                .after(CustomStage::Update)
                .with_system(
                    open.exclusive_system()
                        .with_run_criteria(State::on_enter(ToolState::Open))
                        .in_ambiguity_set(AmbiguitySet),
                )
                .with_system(
                    cleanup
                        .with_run_criteria(State::on_update(ToolState::Open))
                        .in_ambiguity_set(AmbiguitySet),
                ),
        );
    }
}

fn cleanup(mut tool_state: ResMut<State<ToolState>>, mut scan_code_input: ResMut<ScanCodeInput>) {
    scan_code_input.reset_all();
    tool_state.overwrite_replace(ToolState::None).unwrap();
}

fn open(world: &mut World) {
    let path = match FileDialog::new()
        .add_filter("JSON Vismut project", &["json"])
        .set_filename("untitled.json")
        .show_open_single_file()
    {
        Ok(Some(path)) => path,
        Ok(None) => return,
        Err(e) => {
            let message = format!("unable to get export path: {:?}", e);
            warn!("{}", message);
            let mut status_text = world.resource_mut::<StatusText>();
            status_text.set(Urgency::Warn, message);
            return;
        }
    };
    let message = format!("project opened: {:?}", path);

    let mut undo_command_manager = world.resource_mut::<UndoCommandManager>();
    undo_command_manager.push(Box::new(OpenGraph(path)));
    undo_command_manager.push(Box::new(Checkpoint));

    info!("{}", message);
    let mut status_text = world.resource_mut::<StatusText>();
    status_text.set(Urgency::None, message);
}
