// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::new::FileNew;
use crate::undo::undo_command_manager::UndoCommandManager;
use crate::{scan_code_input::ScanCodeInput, ToolState};
use bevy::{app::AppExit, prelude::*};
use bevy_egui::{
    egui::{Area, Ui},
    EguiContext,
};

use super::{canceled_menu, FloatingMenuPos, FloatingMenuState};

/// Returns true if something was clicked.
pub(super) fn file_menu(
    app_exit_events: &mut EventWriter<AppExit>,
    undo_command_manager: &mut UndoCommandManager,
    ui: &mut Ui,
    tool_state: &mut State<ToolState>,
) -> bool {
    if ui.button("New (Ctrl N)").clicked() {
        undo_command_manager.push(Box::new(FileNew));
        true
    } else if ui.button("Save (Ctrl S)").clicked() {
        tool_state.set(ToolState::Save).unwrap();
        true
    } else if ui.button("Open (Ctrl O)").clicked() {
        tool_state.set(ToolState::Open).unwrap();
        true
    } else if ui.button("Export (Ctrl E)").clicked() {
        tool_state.set(ToolState::ExportOutputs(false)).unwrap();
        true
    } else if ui.button("Export As… (Ctrl Shift E)").clicked() {
        tool_state.set(ToolState::ExportOutputs(true)).unwrap();
        true
    } else if ui.button("Export Active… (Ctrl Alt E)").clicked() {
        tool_state.set(ToolState::Export).unwrap();
        true
    } else if ui.button("Quit (Ctrl Q)").clicked() {
        app_exit_events.send(AppExit);
        true
    } else {
        false
    }
}

#[allow(clippy::too_many_arguments)]
pub(super) fn floating_file_menu(
    mut egui_context: ResMut<EguiContext>,
    mut tool_state: ResMut<State<ToolState>>,
    mut floating_menu_state: ResMut<State<FloatingMenuState>>,
    mut undo_command_manager: ResMut<UndoCommandManager>,
    mut app_exit_events: EventWriter<AppExit>,
    i_mouse_button: Res<Input<MouseButton>>,
    scan_code_input: Res<ScanCodeInput>,
    floating_menu_pos: Res<FloatingMenuPos>,
) {
    if *tool_state.current() == ToolState::None {
        Area::new("file_menu")
            .fixed_pos(floating_menu_pos.0)
            .show(egui_context.ctx_mut(), |ui| {
                file_menu(
                    &mut app_exit_events,
                    &mut undo_command_manager,
                    ui,
                    &mut *tool_state,
                );
            });

        if canceled_menu(&*i_mouse_button, &*scan_code_input) {
            let _ = floating_menu_state.set(FloatingMenuState::None);
        }
    }
}
