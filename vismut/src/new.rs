use crate::undo::prelude::UndoCommandManager;
use crate::undo::{UndoCommand, UndoCommandType};
use crate::{clear_graph, World};
use vismut_core::prelude::*;

/// Removes all nodes and edges to create a clean slate.
#[derive(Debug)]
pub struct FileNew;

impl UndoCommand for FileNew {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        undo_command_manager.clear();
        clear_graph(world);

        let new_dag_id = world.resource_mut::<Engine>().insert(Dag::new());
        *world.resource_mut::<DagId>() = new_dag_id;
    }

    fn backward(&self, _world: &mut World, _undo_command_manager: &mut UndoCommandManager) {
        unreachable!("this `UndoCommand` is never saved on the undo stack")
    }
}
