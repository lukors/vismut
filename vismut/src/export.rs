// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::{
    mouse_interaction::active::Active,
    shared::NodeAddressComponent,
    status_bar::{StatusText, Urgency},
    AmbiguitySet, CustomStage, ToolState,
};
use bevy::prelude::*;
use native_dialog::FileDialog;
use std::path::Path;
use vismut_core::pow_two::SizePow2;
use vismut_core::prelude::*;

pub(crate) struct ExportPlugin;

impl Plugin for ExportPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set_to_stage(
            CoreStage::Update,
            SystemSet::new()
                .label(CustomStage::Apply)
                .after(CustomStage::Update)
                .with_system(
                    export
                        .with_run_criteria(State::on_enter(ToolState::Export))
                        .in_ambiguity_set(AmbiguitySet),
                )
                .with_system(
                    cleanup
                        .with_run_criteria(State::on_update(ToolState::Export))
                        .in_ambiguity_set(AmbiguitySet),
                ),
        );
    }
}

fn cleanup(mut tool_state: ResMut<State<ToolState>>) {
    tool_state.overwrite_replace(ToolState::None).unwrap();
}

fn export(
    engine: Res<Engine>,
    q_active: Query<&NodeAddressComponent, With<Active>>,
    mut status_text: ResMut<StatusText>,
) {
    if let Ok(node_address) = q_active.get_single() {
        let slot_address = node_address.0.with_slot_id(SlotId(0));

        let size: SizePow2 = match engine.slot_size(slot_address) {
            Ok(s) => s,
            Err(_) => {
                let message = "unable to get size of slot";
                warn!(message);
                status_text.set(Urgency::Warn, message.to_string());
                return;
            }
        };

        let path = match FileDialog::new()
            .add_filter("PNG Image", &["png"])
            .set_filename("untitled.png")
            .show_save_single_file()
        {
            Ok(path) => path,
            Err(e) => {
                let message = format!("unable to get export path: {:?}", e);
                warn!("{}", message);
                status_text.set(Urgency::Warn, message);
                return;
            }
        };

        let path = match path {
            Some(path) => path,
            None => {
                let message = "invalid export path";
                warn!(message);
                status_text.set(Urgency::Warn, message.to_string());
                return;
            }
        };

        // todo: Make this asynchronous. Maybe use Rust's or Bevy's async stuff to write to disk?
        let texels = match engine.buffer_rgba(slot_address) {
            Ok(buf) => buf,
            Err(e) => {
                let message = format!("error when trying to get pixels from image: {:?}", e);
                error!("{}", message);
                status_text.set(Urgency::Error, message);
                return;
            }
        };

        let (width, height) = size.result();
        let width = width as u32;
        let height = height as u32;
        let buffer = match image::RgbaImage::from_vec(width, height, texels) {
            None => {
                let message = "output image buffer not big enough to contain texels";
                error!(message);
                status_text.set(Urgency::Error, message.to_string());
                return;
            }
            Some(buf) => buf,
        };

        match image::save_buffer(
            &Path::new(&path),
            &buffer,
            width,
            height,
            image::ColorType::Rgba8,
        ) {
            Ok(_) => {
                let message = format!("image exported to {:?}", path);
                info!("{}", message);
                status_text.set(Urgency::None, message);
            }
            Err(e) => {
                error!("{}", e);
                status_text.set(Urgency::Error, format!("{}", e));
            }
        }
    } else {
        let message = "no active node to export";
        warn!("{}", message);
        status_text.set(Urgency::Warn, message.to_string());
    }
}
