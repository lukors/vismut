// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::{
    shared::NodeAddressComponent,
    status_bar::{StatusText, Urgency},
    AmbiguitySet, CustomStage, GridPosition, ToolState,
};
use bevy::prelude::*;
use native_dialog::FileDialog;
use std::io::Write;
use vismut_core::prelude::*;

pub(crate) struct SavePlugin;

impl Plugin for SavePlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set_to_stage(
            CoreStage::Update,
            SystemSet::new()
                .label(CustomStage::Apply)
                .after(CustomStage::Update)
                .with_system(
                    save.with_run_criteria(State::on_enter(ToolState::Save))
                        .in_ambiguity_set(AmbiguitySet),
                )
                .with_system(
                    cleanup
                        .with_run_criteria(State::on_update(ToolState::Save))
                        .in_ambiguity_set(AmbiguitySet),
                ),
        );
    }
}

fn cleanup(mut tool_state: ResMut<State<ToolState>>) {
    tool_state.overwrite_replace(ToolState::None).unwrap();
}

fn save(
    engine: Res<Engine>,
    dag_id: Res<DagId>,
    q_node_positions: Query<(&NodeAddressComponent, &GridPosition)>,
    mut status_text: ResMut<StatusText>,
) {
    let path = match FileDialog::new()
        .add_filter("JSON Vismut project", &["json"])
        .set_filename("untitled.json")
        .show_save_single_file()
    {
        Ok(path) => {
            if let Some(path) = path {
                path
            } else {
                let message = "no export path specified".to_string();
                warn!("{}", message);
                status_text.set(Urgency::Warn, message);
                return;
            }
        }
        Err(e) => {
            let message = format!("unable to get export path: {:?}", e);
            warn!("{}", message);
            status_text.set(Urgency::Warn, message);
            return;
        }
    };

    let mut serialized_dag = SerializeableDag::new();

    serialized_dag.dag = engine
        .dag(*dag_id)
        .expect("there should always be a DAG")
        .clone();
    serialized_dag.positions = q_node_positions
        .iter()
        .filter(|(node_address_component, _)| node_address_component.dag_id() == *dag_id)
        .map(
            |(node_address_component, grid_position)| SerializedPosition {
                node_id: node_address_component.node_id(),
                x: grid_position.x,
                y: grid_position.y,
            },
        )
        .collect::<Vec<_>>();

    let serialized_dag = serde_json::to_string_pretty(&serialized_dag)
        .expect("it should not be possible to fail to serialize a DAG");

    let mut file = std::fs::OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(&path)
        .unwrap();
    file.write_all(serialized_dag.as_bytes()).expect("");

    let message = format!("project saved to {:?}", path);
    info!("{}", message);
    status_text.set(Urgency::None, message);
}
