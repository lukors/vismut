// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;

use crate::{
    sync_graph::GuiEdge, workspace::Workspace, AmbiguitySet, CustomStage, Dragged, ToolState,
};

#[derive(Component, Deref, DerefMut, Default)]
pub struct CustomHitSize(pub Vec2);

#[derive(Component, Default)]
pub(crate) struct Hoverable;

#[derive(Component, Default)]
pub(crate) struct Hovered;

pub(crate) struct HoverablePlugin;

impl Plugin for HoverablePlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set_to_stage(
            CoreStage::Update,
            SystemSet::new()
                .label(CustomStage::Update)
                .after(CustomStage::Setup)
                // Other
                .with_system(
                    hoverable
                        .with_run_criteria(State::on_update(ToolState::None))
                        .in_ambiguity_set(AmbiguitySet),
                ),
        );
    }
}

fn hoverable(
    mut commands: Commands,
    workspace: Res<Workspace>,
    q_hoverable: Query<
        (
            Entity,
            &GlobalTransform,
            &Sprite,
            Option<&CustomHitSize>,
            Option<&GuiEdge>,
        ),
        (With<Hoverable>, Without<Dragged>),
    >,
) {
    if workspace.cursor_moved {
        for (entity, global_transform, sprite, custom_hit_size, gui_edge) in q_hoverable.iter() {
            let size = if let Some(custom_hit_size) = custom_hit_size {
                custom_hit_size.0
            } else if let Some(edge) = gui_edge {
                Vec2::new(edge.start.distance(edge.end), 15.0)
            } else if let Some(size) = sprite.custom_size {
                size
            } else {
                continue;
            };

            // Calculate and apply the offset of the sprite's anchor.
            let offset_fraction = sprite.anchor.as_vec();
            let offset_pixels = -size * offset_fraction;
            let offset_matrix = Mat4::from_translation(offset_pixels.extend(0.0));
            let box_matrix = global_transform.compute_matrix() * offset_matrix;

            let hovered = box_matrix_contains_point(box_matrix, size, workspace.cursor_world);

            if hovered {
                commands.entity(entity).insert(Hovered);
            } else {
                commands.entity(entity).remove::<Hovered>();
            }
        }
    }
}

pub(crate) fn box_contains_point(box_pos: Vec2, box_size: Vec2, point: Vec2) -> bool {
    let half_size = box_size / 2.;

    box_pos.x - half_size.x < point.x
        && box_pos.x + half_size.x > point.x
        && box_pos.y - half_size.y < point.y
        && box_pos.y + half_size.y > point.y
}

pub(crate) fn box_matrix_contains_point(box_matrix: Mat4, box_size: Vec2, point: Vec2) -> bool {
    // box_matrix is the world to object transformation
    // transform the point to object space using the inverse (object to world)
    let p = box_matrix.inverse().mul_vec4(point.extend(0.0).extend(1.0));
    let half_size = box_size / 2.;
    // no need for a translation or rotation as we're in box space
    p.x <= half_size.x && p.x >= -half_size.x && p.y <= half_size.y && p.y >= -half_size.y
}
