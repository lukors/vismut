# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
### Added
- Reduced CPU usage when not in focus to about one fifth
- Improved latency
- `New`, `Save`, and `Open` commands
- Click connections and press delete to delete them
- New node design
- Nodes exist on a grid

## [0.5.0] - 2022-04-23
### Changed
- Full rewrite of Vismut Core
- Sizes are now forced to powers of two
- Minor changes to the properties panel
- Changed names of nodes: "Separate" -> "Split", "Combine" -> "Merge", "Value" -> "Grayscale"
- Thumbnails look a bit worse and use more RAM, to be fixed in a future version

### Removed
- `View` -> `Stats`, coming back in a future version
- RAM cap, coming back in a future version

## [0.4.0] - 2022-02-05
### Added
- Menu bar with `File`, `Add Node`, `View`, and `Help` menus
- Status bar in bottom left
- Properties panel on the right
- Debug stats under `View` -> `Stats`
- Dot grid in background
- The user can't scroll away from the graph anymore
- Updated all visuals
- Screen space grid behind alpha channels

### Removed
- Instructional text in top left
- Edit mode, replaced with properties panel

### Fixed
- Fixed crawling text by turning off MSAA

## [0.3.0] - 2022-01-06
### Added
- Export output nodes
- Name output nodes

### Changed
- A ton of things, the changelog for this version is not comprehensive

### Removed
- Deactivated tools: Grab tool on `G`, first person mode on ~, probably more
- Deactivated nodes: Mix, Height To Normal, probably more

### Fixed
- Much improved stability
- Panning speed when window scale is other than 1.0

## [0.2.1] - 2021-05-12
### Added
- Mix nodes can now have more than two inputs

### Fixed
- Fixed crash when processing a mix node with only one input

## [0.2.0] - 2021-05-05
### Added
- Can now edit the Resize Policy and Resize Filter of nodes with `Tab`
- Added Mix node
- Added Value node

## [0.1.1] - 2021-04-24
### Added
- Delete selected nodes with `X` or `delete`
- Box select by dragging with `left mouse button` on empty space
- Pan view with `middle mouse button` and drag
- Drag on node to grab all selected nodes

### Changed
- First person hotkey from `` Shift ` `` to just `` ` `` (the tilde `~` key)
- Moving things around with drag and drop is now a bit nicer

### Fixed
- File dialogs should work properly on Windows now
- Export hotkey on Linux `Shift Alt S` is less janky

## [0.1.0] - 2021-04-21
### Added
- Initial release

[Unreleased]: https://gitlab.com/vismut-org/vismut/-/compare/v0.5.0...main
[0.5.0]: https://gitlab.com/vismut-org/vismut/-/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/vismut-org/vismut/-/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/vismut-org/vismut/-/compare/v0.2.1...v0.3.0
[0.2.1]: https://gitlab.com/vismut-org/vismut/-/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/vismut-org/vismut/-/compare/v0.1.1...v0.2.0
[0.1.1]: https://gitlab.com/vismut-org/vismut/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/vismut-org/vismut/-/commits/v0.1.0/
