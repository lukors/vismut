# Release Checklist

This document contains a Markdown checklist that can be copied into an issue on GitLab to ensure no step is forgotten in the process of releasing a new version of Vismut.

It will change with the needs of the project.

```txt
## Prepare release
- Release commit
    - [ ] Update MANUAL.md
    - [ ] Update CHANGELOG.md
    - [ ] Update README.md
    - [ ] Update version in Cargo.toml
    - [ ] Take new screenshot
    - [ ] Create commit
- [ ] `git tag vx.y.z`
- [ ] `git push upstream --tags`

## Build binaries
- [ ] Checkout tag and build on Windows
    - [ ] Package it
    - [ ] Test it
    - [ ] Upload it
- [ ] Checkout tag and build on Linux
    - [ ] Package it
    - [ ] Test it
    - [ ] Upload it

## Release
- [ ] Create release on GitLab
- [ ] Paste changelog in release notes
- [ ] Announce
```
