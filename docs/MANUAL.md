# Vismut Manual for v0.5

Vismut currently has one use case implemented: manually packing image channels.

Notable features that are not yet implemented:

- Copy/paste & duplicating nodes
- Saving/loading (you can import and export images, but not save the node graphs
  you create)
- Easily disconnecting slots, the only way is to `Ctrl` `Left click` and drag
  the slot and drop the line on empty space

## Community

Join the [Vismut Zulip](https://vismut.zulipchat.com) if you have any questions
or want to talk to other users, welcome! 😃

## Controls

- `Middle click` and drag: Pan
- `Left click` and drag on empty space: Box select
- `Left click` and drag on node: Move selected nodes, or the hovered node if it
  is not selected
- `Left click` and drag on slot: Start creating a connection, drop it on another
  slot to connect them
- `Ctrl` `Left click` and drag on a slot: Grab that end of any line that's
  connected to the slot, drop it on empty space to disconnect it, **this is the
  only way to disconnect a slot**
- `G`: Grab and move selected nodes
- `Escape`: Cancel active tool

All other controls are shown alongside each function in the dropdown menus in
the menu bar along the top.

## Node Properties

The active node's properties can be edited in the properties panel on the right side of the screen. Click on a node to make it active, indicated by an aqua border. Some node
types have special properties, but all node types have the following:

- `Resize Policy` - how the size of the node's outputs are decided.
- `Resize Filter` - if resizing should use nearest neighbour or triangle (AKA
  linear)
  filtering when resizing.

Each node's special properties are detailed under the "Node Types" heading.

## Packing Channels

1. Create one or more `Image` nodes to import your starting images, all nodes
   are created from the **Add Node** menu at the top.
1. Plug each `Image` node into a `Separate` node to expose each individual
   channel of the image.
1. Create one `Combine` node for each output image you want.
1. Plug the outputs from the `Separate` node(s) into the `Combine` node(s) to
   pack them how you want. The channels are laid out from top to bottom in this
   order: `Red`, `Green`, `Blue`,
   `Alpha`.
1. Export the image(s) as instructed below.

## Exporting images

- If you plan on exporting a single image once: Make the node you want to export
  active by clicking on it, so it has an aqua border, and press `File`
  ➤`Export Active…`.
- If you want to export several images, or the same image several times:
    1. Create an `Output` node for each image you want to export and plug them
       in.
    1. Set each `Output` node's name by clicking them and setting the name in
       the properties panel on the right.
    1. Press `File`➤`Export` and select a folder to export to.

Images are currently always exported as PNG. More export options will be added in the future.

## Node Types

### Shared Properties

These properties are used by several node types.

- Size From: Decides how the node's size is determined. The size is always a power of two.
  - Input: Calculated as an offset from the size of the first input from the top.
  - Absolute: A specific width and height.
- Resize Filter: How the color of each pixel is chosen when resizing.
  - Nearest: Take the closest pixel from the source image, causes a pixelated look when scaling up, and a noisy look when scaling down.
  - Bilinear: Linearly interpolates when scaling up, and averages the colors when scaling down. Creates a smoother look.

### Image

Imports an image. The image does not automatically update when the source image is changed. The file window that lets you pick the image actually allows you to pick several images at once.

- Path: The path the image was loaded from, can not be changed.

### Split

Splits an RGBA input into 4 grayscale outputs.

### Merge

Merges 4 grayscale inputs into an RGBA output. R, G, and B defaults to black if
they have no input, and alpha defaults to white.

### Output

Exports an RGBA input to a PNG image on disk when `File`➤`Export` is pressed.
Grayscale images can not be exported yet.

**Properties**

- Name: The name of the created PNG image, the ".png" at the end is added
  automatically on export.

### Grayscale

Outputs a grayscale channel containing a single pixel with the given value.

**Properties**

- Value: The value of the output pixel.
