# Vismut Contribution Process

The **Vismut** and **Vismut Core** projects use a process called the **Vismut Contribution Process** (VCP).
This document gets straight to the point and only lists the facts of how things work,
this makes it useful as a quick reference of all the key points.
[PROCESS_EXPLAINED.md](PROCESS_EXPLAINED.md) explains why the process is how it is,
and goes into more detail with examples and explanations.

## Issue Tracker

Here is how the issue tracker works.

- The title of an issue must describe a problem
- If it turns out after some discussion that an issue can't be described as a problem,
  the issue is closed
- An issue without any activity for over a year is closed

## Merge Requests

Here is how merge requests work.

- Anyone can submit a merge request
- Only a valid merge request is merged
- Only a maintainer can merge a merge request
- A maintainer is not allowed to merge their own merge request unless it has been without activity for 48 hours
- A merge request without any activity for over a year is closed 

A merge request is valid if it:

- States the problem it's solving in the title
- States how it's solving the stated problem at the beginning of its description
- Is a solution to the stated problem,
  and nothing more
- Passes the continuous integration (CI)

## Roles

Here are the different roles in the VCP,
and what they do.
You can have more than one role at a time.

### Maintainer

A maintainer's job is to:

- Enforce the VCP
- Merge merge requests

A maintainer gets the "developer" role in the Vismut organization,
so they can merge people's merge requests.

Someone might be invited to get the maintainer role if they do all of these things:

- Have shown through their contributions that they understand the VCP
- Have shown that they understand the goals of the project

A maintainer will have their role removed if they do any of these things:

- Repeatedly fail to enforce the VCP properly.
- Have been inactive in the community for more than a year.

### Moderator

A moderator's job is to:

- Enforce the [Vismut Code of Conduct](../CODE_OF_CONDUCT.md)
- Behave according to the spirit of the "encouraged behavior" in the code of conduct

This is what a moderator gets:

- The "moderator" role on Zulip
- Access to #moderation on Zulip.
  There they record warnings,
  and request people to be banned by an administrator (only an administrator can ban people)

Someone might be invited to get the moderator role if they do all of these things:

- Behave according to the spirit of the "encouraged behavior" in the code of conduct
- Have been active for a while

A moderator will have their role removed if they do any of these things:

- Repeatedly enforce the code of conduct in a wrong way
- Behave in an unacceptable way as described in the code of conduct
- Have been inactive in the community for more than a year

### Administrator

An administrator's job is to enable people to work on the project. This means doing things such as:

- Ensuring there are spaces for people to collaborate on the project
- Inviting appropriate people to become maintainers and moderators
- Ensuring there are enough administrators to keep the project going by promoting the most dedicated maintainers and moderators
- Make final decisions
