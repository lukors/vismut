use std::{fs::create_dir, path::Path};
use vismut_core::address::SlotAddress;
use vismut_core::prelude::*;

const DIR_OUT: &str = "out";
const DIR_CMP: &str = "data/test_compare";
const IMAGE_2: &str = "data/image_2.png";
const IMAGE_2_NON_POW_2: &str = "data/image_2_non_pow_2.png";

fn ensure_out_dir() {
    let _ = create_dir(Path::new(DIR_OUT));
}

fn images_equal<P: AsRef<Path>, Q: AsRef<Path>>(path_1: P, path_2: Q) -> bool {
    let image_1 = image::open(path_1).unwrap();
    let raw_pixels_1 = image_1.as_flat_samples_u8().unwrap().samples;
    let image_2 = image::open(path_2).unwrap();
    let raw_pixels_2 = image_2.as_flat_samples_u8().unwrap().samples;

    raw_pixels_1.iter().eq(raw_pixels_2.iter())
}

fn save_and_compare(engine: &mut Engine, slot_address: SlotAddress, name: &str) {
    engine.prepare();
    engine.run_until_done();

    let (path_out, path_cmp) = build_paths(name);
    save_buffer_rgba(engine, slot_address, Path::new(&path_out));

    assert!(images_equal(path_out, path_cmp));
}

fn build_paths(name: &str) -> (String, String) {
    (
        format!("{}/{}", DIR_OUT, name),
        format!("{}/{}", DIR_CMP, name),
    )
}

fn engine_new() -> Engine {
    Engine::new()
}

fn save_buffer_rgba(engine: &Engine, slot_address: SlotAddress, path: &Path) {
    let (width, height) = engine.slot_size(slot_address).unwrap().to_tuple();
    let (width, height) = (width.result() as u32, height.result() as u32);

    ensure_out_dir();
    image::save_buffer(
        path,
        &image::RgbaImage::from_vec(
            width,
            height,
            engine
                .buffer_rgba(slot_address)
                .expect("failed getting buffer"),
        )
        .expect("failed converting buffer to `RgbaImage`"),
        width,
        height,
        image::ColorType::Rgba8,
    )
    .expect("failed saving buffer");
}

#[test]
fn input_output() {
    let mut dag = Dag::new();
    let image = dag.insert(NodeType::Image(ImageNode(IMAGE_2.into())));
    let output_rgba = dag.insert(NodeType::OutputRgba(OutputRgbaNode("out".into())));

    dag.connect(image, SlotId(0), output_rgba, SlotId(0))
        .unwrap();

    let mut engine = engine_new();
    let dag_id = engine.insert(dag);

    save_and_compare(
        &mut engine,
        SlotAddress::new(dag_id, output_rgba, SlotId(0)),
        "input_output.png",
    );
}

#[test]
fn load_graph_input_output() {
    let mut engine = engine_new();

    let file = std::fs::File::open("data/input_output.json").unwrap();
    let reader = std::io::BufReader::new(file);
    let serialized_dag: SerializeableDag = serde_json::de::from_reader(reader).unwrap();
    let dag_id = engine.insert(serialized_dag.dag);

    let output_slot = *engine.output_addresses(dag_id).unwrap().first().unwrap();

    save_and_compare(&mut engine, output_slot, "load_input_output.png");
}

#[test]
fn value_node() {
    let mut dag = Dag::new();

    let red_node = dag.insert(NodeType::Grayscale(GrayscaleNode(0.0)));
    let green_node = dag.insert(NodeType::Grayscale(GrayscaleNode(0.33)));
    let blue_node = dag.insert(NodeType::Grayscale(GrayscaleNode(0.66)));
    let alpha_node = dag.insert(NodeType::Grayscale(GrayscaleNode(1.0)));
    let combine_node = dag.insert(NodeType::MergeRgba(MergeRgbaNode(Resize::default())));

    dag.connect(red_node, SlotId(0), combine_node, SlotId(0))
        .unwrap();
    dag.connect(green_node, SlotId(0), combine_node, SlotId(1))
        .unwrap();
    dag.connect(blue_node, SlotId(0), combine_node, SlotId(2))
        .unwrap();
    dag.connect(alpha_node, SlotId(0), combine_node, SlotId(3))
        .unwrap();

    let mut engine = Engine::new();
    let dag_id = engine.insert(dag);

    save_and_compare(
        &mut engine,
        SlotAddress::new(dag_id, combine_node, SlotId(0)),
        "value_node.png",
    );
}

#[test]
fn split_merge() {
    let mut dag = Dag::new();

    let image_node = dag.insert(NodeType::Image(ImageNode(IMAGE_2.into())));
    let split_node = dag.insert(NodeType::SplitRgba(SplitRgbaNode(Resize::default())));
    let merge_node = dag.insert(NodeType::MergeRgba(MergeRgbaNode(Resize::default())));

    dag.connect(image_node, SlotId(0), split_node, SlotId(0))
        .unwrap();
    dag.connect(split_node, SlotId(0), merge_node, SlotId(1))
        .unwrap();
    dag.connect(split_node, SlotId(1), merge_node, SlotId(2))
        .unwrap();
    dag.connect(split_node, SlotId(2), merge_node, SlotId(3))
        .unwrap();
    dag.connect(split_node, SlotId(3), merge_node, SlotId(0))
        .unwrap();

    let mut engine = Engine::new();
    let dag_id = engine.insert(dag);

    save_and_compare(
        &mut engine,
        SlotAddress::new(dag_id, merge_node, SlotId(0)),
        "split_merge.png",
    );
}

#[test]
fn edit_dag() {
    let mut engine = Engine::new();
    let dag_id = engine.insert(Dag::new());

    let split_node = engine
        .insert_node(
            dag_id,
            NodeType::SplitRgba(SplitRgbaNode(Resize::default())),
        )
        .unwrap();
    engine.prepare();
    engine.run_until_done();
    let image_node = engine
        .insert_node(dag_id, NodeType::Image(ImageNode(IMAGE_2.into())))
        .unwrap();
    let merge_node = engine
        .insert_node(
            dag_id,
            NodeType::MergeRgba(MergeRgbaNode(Resize::default())),
        )
        .unwrap();

    engine
        .connect(Edge::new(
            image_node.with_slot_id(SlotId(0)),
            split_node.with_slot_id(SlotId(0)),
        ))
        .unwrap();

    engine
        .connect(Edge::new(
            split_node.with_slot_id(SlotId(0)),
            merge_node.with_slot_id(SlotId(3)),
        ))
        .unwrap();
    engine
        .connect(Edge::new(
            split_node.with_slot_id(SlotId(1)),
            merge_node.with_slot_id(SlotId(2)),
        ))
        .unwrap();

    engine
        .connect(Edge::new(
            split_node.with_slot_id(SlotId(2)),
            merge_node.with_slot_id(SlotId(1)),
        ))
        .unwrap();
    engine
        .connect(Edge::new(
            split_node.with_slot_id(SlotId(3)),
            merge_node.with_slot_id(SlotId(0)),
        ))
        .unwrap();

    save_and_compare(
        &mut engine,
        merge_node.with_slot_id(SlotId(0)),
        "edit_dag.png",
    );
}

#[test]
fn remove_node() {
    let mut engine = Engine::new();
    let dag_id = engine.insert(Dag::new());

    let grayscale_node = engine
        .insert_node(dag_id, NodeType::Grayscale(GrayscaleNode(0.5)))
        .unwrap();
    let merge_node = engine
        .insert_node(
            dag_id,
            NodeType::MergeRgba(MergeRgbaNode(Resize::default())),
        )
        .unwrap();

    engine
        .connect(Edge::new(
            grayscale_node.with_slot_id(SlotId(0)),
            merge_node.with_slot_id(SlotId(0)),
        ))
        .unwrap();

    engine.prepare();
    engine.remove_node(grayscale_node).unwrap();
    engine.prepare();
}

#[test]
fn force_pow_2_image() {
    let mut dag = Dag::new();
    let image = dag.insert(NodeType::Image(ImageNode(IMAGE_2_NON_POW_2.into())));

    let mut engine = engine_new();
    let dag_id = engine.insert(dag);

    save_and_compare(
        &mut engine,
        SlotAddress::new(dag_id, image, SlotId(0)),
        "force_pow_2.png",
    );
}

#[test]
fn connect_between_dags() {
    let mut engine = engine_new();
    let dag_a = engine.insert(Dag::new());
    let dag_b = engine.insert(Dag::new());

    let node_a = engine
        .insert_node(dag_a, NodeType::Image(ImageNode(IMAGE_2.into())))
        .unwrap();
    let node_b = engine
        .insert_node(
            dag_b,
            NodeType::OutputRgba(OutputRgbaNode("out".to_string())),
        )
        .unwrap();

    let node_a_slot = node_a.with_slot_id(SlotId(0));
    let node_b_slot = node_b.with_slot_id(SlotId(0));

    engine.connect(Edge::new(node_a_slot, node_b_slot)).unwrap();

    save_and_compare(&mut engine, node_b_slot, "connect_between_dags.png");
}
