use serde::{Deserialize, Serialize};

use crate::{
    address::{NodeAddress, Side, SlotId},
    live::node::GrayscaleLiveNode,
    node_definition::NodeDefinition,
    slot::{Slot, SlotType},
};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct GrayscaleNode(pub f32);
impl NodeDefinition for GrayscaleNode {
    fn slots(&self, side: Side) -> Vec<Slot> {
        if side == Side::Input {
            Vec::new()
        } else {
            vec![Slot::new(SlotType::Gray, SlotId(0), "output")]
        }
    }

    fn activate(&self, live_dag: &mut crate::live::dag::LiveDag, creator_address: NodeAddress) {
        let live_node_id = live_dag.insert(creator_address, GrayscaleLiveNode(self.0).into());
        live_dag.map_corresponding(creator_address, live_node_id, Side::Output, SlotId(0));
    }

    fn title(&self) -> String {
        "Grayscale".into()
    }
}
