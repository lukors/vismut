use serde::{Deserialize, Serialize};
use std::path::PathBuf;

use crate::{
    address::{NodeAddress, Side, SlotId},
    live::node::ImageLiveNode,
    node_definition::NodeDefinition,
    slot::{Slot, SlotType},
};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct ImageNode(pub PathBuf);
impl NodeDefinition for ImageNode {
    fn slots(&self, side: Side) -> Vec<Slot> {
        if side == Side::Input {
            Vec::new()
        } else {
            vec![Slot::new(SlotType::Rgba, SlotId(0), "output")]
        }
    }

    fn activate(&self, live_dag: &mut crate::live::dag::LiveDag, creator_address: NodeAddress) {
        let live_node_id =
            live_dag.insert(creator_address, ImageLiveNode(self.0.to_path_buf()).into());
        live_dag.map_corresponding(creator_address, live_node_id, Side::Output, SlotId(0));
    }

    fn title(&self) -> String {
        "Image".into()
    }
}
