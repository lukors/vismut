use serde::{Deserialize, Serialize};

use crate::{
    address::{NodeAddress, Side, SlotId},
    live::node::OutputRgbaLiveNode,
    node_definition::NodeDefinition,
    slot::{Slot, SlotType},
};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct OutputRgbaNode(pub String);
impl NodeDefinition for OutputRgbaNode {
    fn slots(&self, side: Side) -> Vec<Slot> {
        if side == Side::Input {
            vec![Slot::new(SlotType::Rgba, SlotId(0), "input")]
        } else {
            Vec::new()
        }
    }

    fn activate(&self, live_dag: &mut crate::live::dag::LiveDag, creator_address: NodeAddress) {
        let live_node_id = live_dag.insert(creator_address, OutputRgbaLiveNode.into());
        live_dag.map_corresponding(creator_address, live_node_id, Side::Input, SlotId(0));
        live_dag.map_corresponding(creator_address, live_node_id, Side::Output, SlotId(0));
    }

    fn name(&self) -> crate::error::Result<&String> {
        Ok(&self.0)
    }

    fn set_name(&mut self, new_name: String) -> crate::error::Result<()> {
        self.0 = new_name;
        Ok(())
    }

    fn title(&self) -> String {
        "Output".into()
    }
}
