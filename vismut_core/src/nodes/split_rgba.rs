use serde::{Deserialize, Serialize};

use crate::{
    address::{NodeAddress, Side, SlotId},
    live::{
        edge::LiveEdge,
        node::{ResizeLiveNode, SplitRgbaLiveNode},
    },
    node_definition::NodeDefinition,
    prelude::Resize,
    slot::{Slot, SlotType},
};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct SplitRgbaNode(pub Resize);

impl NodeDefinition for SplitRgbaNode {
    fn slots(&self, side: Side) -> Vec<Slot> {
        if side == Side::Input {
            vec![Slot::new(SlotType::Rgba, SlotId(0), "input")]
        } else {
            vec![
                Slot::new(SlotType::Gray, SlotId(0), "red"),
                Slot::new(SlotType::Gray, SlotId(1), "green"),
                Slot::new(SlotType::Gray, SlotId(2), "blue"),
                Slot::new(SlotType::Gray, SlotId(3), "alpha"),
            ]
        }
    }

    fn activate(&self, live_dag: &mut crate::live::dag::LiveDag, creator_address: NodeAddress) {
        const RESIZE_SLOT_COUNT: usize = 4;

        let resize_node_id = {
            let node_type = ResizeLiveNode {
                count: RESIZE_SLOT_COUNT,
                resize: self.0,
                default: vec![0.0, 0.0, 0.0, 1.0],
            }
            .into();
            live_dag.insert(creator_address, node_type)
        };

        let split_rgba_node_id = {
            let live_node_id = live_dag.insert(creator_address, SplitRgbaLiveNode.into());
            live_dag.map_corresponding(creator_address, live_node_id, Side::Input, SlotId(0));

            live_node_id
        };

        // Connect `SplitRgba` slots to `Resize` slots.
        for i in 0..RESIZE_SLOT_COUNT {
            let slot_id = SlotId(i);
            let resize_slot_address = resize_node_id.with_slot_id(slot_id);
            let split_rgba_address = split_rgba_node_id.with_slot_id(slot_id);
            let live_edge = LiveEdge::new(split_rgba_address, resize_slot_address);
            live_dag.add_edge(live_edge);

            live_dag.map_corresponding(creator_address, resize_node_id, Side::Output, slot_id);
        }
    }

    fn resize(&self) -> Option<Resize> {
        Some(self.0)
    }

    fn resize_mut(&mut self) -> Option<&mut Resize> {
        Some(&mut self.0)
    }

    fn title(&self) -> String {
        "Split".into()
    }
}
