use serde::{Deserialize, Serialize};

use crate::{
    address::{NodeAddress, Side, SlotId},
    live::{
        edge::LiveEdge,
        node::{MergeRgbaLiveNode, ResizeLiveNode},
    },
    node_definition::NodeDefinition,
    prelude::Resize,
    slot::{Slot, SlotType},
};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct MergeRgbaNode(pub Resize);

impl NodeDefinition for MergeRgbaNode {
    fn slots(&self, side: Side) -> Vec<Slot> {
        if side == Side::Input {
            vec![
                Slot::new(SlotType::Gray, SlotId(0), "red"),
                Slot::new(SlotType::Gray, SlotId(1), "green"),
                Slot::new(SlotType::Gray, SlotId(2), "blue"),
                Slot::new(SlotType::Gray, SlotId(3), "alpha"),
            ]
        } else {
            vec![Slot::new(SlotType::Rgba, SlotId(0), "output")]
        }
    }

    fn activate(&self, live_dag: &mut crate::live::dag::LiveDag, creator_address: NodeAddress) {
        const RESIZE_SLOT_COUNT: usize = 4;

        let resize_node_id = {
            let node_type = ResizeLiveNode {
                count: RESIZE_SLOT_COUNT,
                resize: self.0,
                default: vec![0.0, 0.0, 0.0, 1.0],
            }
            .into();
            live_dag.insert(creator_address, node_type)
        };

        let merge_rgba_node_id = {
            let live_node_id = live_dag.insert(creator_address, MergeRgbaLiveNode.into());
            live_dag.map_corresponding(creator_address, live_node_id, Side::Output, SlotId(0));

            live_node_id
        };

        // Connect `Resize` slots to `MergeRgba` slots.
        for i in 0..RESIZE_SLOT_COUNT {
            let slot_id = SlotId(i);
            let resize_slot_address = resize_node_id.with_slot_id(slot_id);
            let merge_rgba_address = merge_rgba_node_id.with_slot_id(slot_id);
            let live_edge = LiveEdge::new(resize_slot_address, merge_rgba_address);
            live_dag.add_edge(live_edge);

            live_dag.map_corresponding(creator_address, resize_node_id, Side::Input, slot_id);
        }
    }

    fn title(&self) -> String {
        "Merge".into()
    }

    fn resize(&self) -> Option<Resize> {
        Some(self.0)
    }

    fn resize_mut(&mut self) -> Option<&mut Resize> {
        Some(&mut self.0)
    }
}
