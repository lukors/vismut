use crate::address::{NodeId, Side, SlotAddress, SlotAddressSide, SlotId};
use crate::error::{Result, VismutError};
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

/// A connection between two nodes in a `Dag`.
#[derive(Debug, Copy, Clone, Default, Eq, PartialEq)]
pub struct Edge {
    pub slot_address_output: SlotAddress,
    pub slot_address_input: SlotAddress,
}

impl Display for Edge {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "(output: {}, input: {})",
            self.slot_address_output, self.slot_address_input
        )
    }
}

impl Edge {
    pub const fn new(slot_address_output: SlotAddress, slot_address_input: SlotAddress) -> Self {
        Self {
            slot_address_output,
            slot_address_input,
        }
    }

    pub fn from_sided(
        slot_address_side_a: SlotAddressSide,
        slot_address_side_b: SlotAddressSide,
    ) -> Result<Self> {
        if slot_address_side_a.node_id == slot_address_side_b.node_id {
            Err(VismutError::InvalidNodeId(slot_address_side_a.node_id))
        } else if slot_address_side_a.side == slot_address_side_b.side {
            Err(VismutError::InvalidSide)
        } else {
            let (slot_address_output, slot_address_input) =
                if let Side::Output = slot_address_side_a.side {
                    (
                        slot_address_side_a.without_side(),
                        slot_address_side_b.without_side(),
                    )
                } else {
                    (
                        slot_address_side_b.without_side(),
                        slot_address_side_a.without_side(),
                    )
                };

            Ok(Self {
                slot_address_output,
                slot_address_input,
            })
        }
    }
}

/// An `Edge` without a `DagId`.
///
/// This is useful when a `Dag` hasn't been inserted into an `Engine`.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct DagEdge {
    pub node_id_output: NodeId,
    pub slot_id_output: SlotId,
    pub node_id_input: NodeId,
    pub slot_id_input: SlotId,
}

impl Display for DagEdge {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "(OUTPUT node id: {}, slot id: {}, INPUT node id: {} slot id: {})",
            self.node_id_output, self.slot_id_output, self.node_id_input, self.slot_id_input
        )
    }
}

impl From<Edge> for DagEdge {
    fn from(edge: Edge) -> Self {
        Self {
            node_id_output: edge.slot_address_output.node_id,
            slot_id_output: edge.slot_address_output.slot_id,
            node_id_input: edge.slot_address_input.node_id,
            slot_id_input: edge.slot_address_input.slot_id,
        }
    }
}

impl DagEdge {
    pub const fn new(
        node_id_output: NodeId,
        slot_id_output: SlotId,
        node_id_input: NodeId,
        slot_id_input: SlotId,
    ) -> Self {
        Self {
            node_id_output,
            slot_id_output,
            node_id_input,
            slot_id_input,
        }
    }
}
