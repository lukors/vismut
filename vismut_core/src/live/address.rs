use crate::address::{Side, SlotId};
use std::fmt::{Display, Formatter};

/// Points to a specific slot on a specific live node, without considering which side the slot is
/// on.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct LiveSlotAddress {
    pub live_node_id: LiveNodeId,
    pub slot_id: SlotId,
}

impl Display for LiveSlotAddress {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "(LiveNodeId: {}, SlotId: {})",
            self.live_node_id, self.slot_id
        )
    }
}

impl LiveSlotAddress {
    pub fn with_side(&self, side: Side) -> LiveSlotAddressSide {
        LiveSlotAddressSide {
            live_node_id: self.live_node_id,
            side,
            slot_id: self.slot_id,
        }
    }
}

/// Points to a specific slot on a specific side of a specific live node.
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct LiveSlotAddressSide {
    pub live_node_id: LiveNodeId,
    pub side: Side,
    pub slot_id: SlotId,
}

impl Display for LiveSlotAddressSide {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "(LiveNodeId: {}, Side: {}, SlotId: {})",
            self.live_node_id, self.side, self.slot_id
        )
    }
}

impl LiveSlotAddressSide {
    pub fn without_side(self) -> LiveSlotAddress {
        LiveSlotAddress {
            live_node_id: self.live_node_id,
            slot_id: self.slot_id,
        }
    }
}

/// The ID of a node in the flattened "live" world.
#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct LiveNodeId(pub usize);

impl Display for LiveNodeId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl LiveNodeId {
    pub fn with_slot_id(self, slot_id: SlotId) -> LiveSlotAddress {
        LiveSlotAddress {
            live_node_id: self,
            slot_id,
        }
    }
}
