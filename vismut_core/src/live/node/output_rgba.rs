use std::sync::Mutex;

use threadpool::ThreadPool;

use crate::address::{Side, SlotId};
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::pow_two::SizePow2;
use crate::slot::{Slot, SlotType};

use super::{LiveNodeDefinition, LiveNodeType};

#[derive(Debug, PartialEq, Eq)]
pub struct OutputRgbaLiveNode;
impl From<OutputRgbaLiveNode> for LiveNodeType {
    fn from(x: OutputRgbaLiveNode) -> Self {
        LiveNodeType::OutputRgba(x)
    }
}
impl LiveNodeDefinition for OutputRgbaLiveNode {
    fn slots(&self, side: Side) -> Vec<Slot> {
        if let Side::Input = side {
            vec![Slot::new(SlotType::Rgba, SlotId(0), "input")]
        } else {
            vec![Slot::new(SlotType::Rgba, SlotId(0), "output")]
        }
    }

    fn process(&self, _thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        process(process_data)
    }
}

pub(super) fn process(process_data: ProcessData) {
    let slot_image = process_data.get_slot_images(&[SlotId(0)])[0];

    let slot_image = if let Some(slot_image) = slot_image {
        slot_image.clone()
    } else {
        SlotImage::rgba_from_values(SizePow2::default(), [0.0, 0.0, 0.0, 1.0])
            .expect("could not create SlotImage")
    };

    let slot_image = vec![(SlotId(0), slot_image)];
    process_data.send(slot_image).unwrap();
}
