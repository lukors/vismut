mod grayscale;
mod image;
mod merge_rgba;
mod output_rgba;
mod resize;
mod split_rgba;

use crate::address::Side;
use crate::address::{NodeAddress, SlotId};
use crate::engine::VismutPixel;
use crate::error::{Result, VismutError};
use crate::live::address::LiveNodeId;
use crate::live::slot_data::SlotImage;
use crate::prelude::Buffer;
use std::sync::{mpsc, Mutex};

use crate::slot::Slot;
use std::fmt::{Display, Formatter};
use threadpool::ThreadPool;

pub(crate) use self::grayscale::GrayscaleLiveNode;
pub(crate) use self::image::ImageLiveNode;
pub(crate) use self::merge_rgba::MergeRgbaLiveNode;
pub(crate) use self::output_rgba::OutputRgbaLiveNode;
pub(crate) use self::resize::ResizeLiveNode;
pub(crate) use self::split_rgba::SplitRgbaLiveNode;

#[derive(Debug)]
pub(crate) struct ProcessData {
    pub sender: mpsc::Sender<FinishedProcessing>,
    pub live_node_id: LiveNodeId,
    pub slot_images: Vec<(SlotId, SlotImage)>,
}

impl ProcessData {
    pub fn send(self, slot_images: Vec<(SlotId, SlotImage)>) -> Result<()> {
        self.sender
            .send(FinishedProcessing {
                live_node_id: self.live_node_id,
                slot_images,
            })
            .map_err(|_| VismutError::SendError)
    }

    /// Takes a list of `SlotId`s and returns a list of their corresponding `SlotImage`s. It returns
    /// `None` for each `SlotImage` that does not exist.
    pub fn get_slot_images(&self, slot_ids: &[SlotId]) -> Vec<Option<&SlotImage>> {
        let mut output = Vec::with_capacity(slot_ids.len());

        for slot_id in slot_ids {
            let slot_image = self
                .slot_images
                .iter()
                .find(|(slot_id_cmp, _)| slot_id_cmp == slot_id)
                .map(|(_, slot_image)| slot_image);
            output.push(slot_image);
        }

        output
    }
}

#[derive(Debug, PartialEq)]
pub(crate) struct FinishedProcessing {
    pub live_node_id: LiveNodeId,
    pub slot_images: Vec<(SlotId, SlotImage)>,
}

#[derive(Debug, PartialEq)]
pub(crate) struct LiveNode {
    pub creator_address: NodeAddress,
    pub live_node_id: LiveNodeId,
    pub live_node_type: LiveNodeType,
}

// todo: add function that returns how many inputs the node expects

/// This is the processable representation of a type of node. As opposed the `NodeType`, this enum
/// is made for processing. This means that this type is made behave more similarly. For instance,
/// output nodes have an output slot that image data can be saved on, they don't have the huge
/// special case of nested dags, as each dag node is flattened into their contents in
/// `Engine.prepare()`, and so on.
#[derive(Debug, PartialEq)]
pub enum LiveNodeType {
    /// Reads the specified image from disk. If the image's size is not a power of two it will be
    /// resized to the closest power of two on each axis.
    Image(ImageLiveNode),
    OutputRgba(OutputRgbaLiveNode),
    /// Can be used both to simply output a grayscale buffer with the specified value on
    /// every pixel, and as a "default" value for something else. If it has something
    /// plugged into its input slot, it will use that instead of creating a new buffer.
    Grayscale(GrayscaleLiveNode),
    /// Takes 1 rgba image and splits it into 4 grayscale images.
    SplitRgba(SplitRgbaLiveNode),
    /// Takes 4 grayscale images and combines them into one rgba image.
    MergeRgba(MergeRgbaLiveNode),
    /// Resize `count` number of grayscale inputs to the size determined by the `resize_policy`,
    /// using the `resize_filter` filter. It defaults to a black buffer if it has no input, so use a
    /// `Self::Grayscale` node if you want to control that value.
    Resize(ResizeLiveNode),
}

pub(crate) trait LiveNodeDefinition {
    fn slots(&self, side: Side) -> Vec<Slot>;
    fn process(&self, thread_pool: &Mutex<ThreadPool>, process_data: ProcessData);
}

impl Display for LiveNodeType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl LiveNodeType {
    pub(crate) fn get_definition(&self) -> &dyn LiveNodeDefinition {
        match self {
            LiveNodeType::Image(x) => x,
            LiveNodeType::OutputRgba(x) => x,
            LiveNodeType::Grayscale(x) => x,
            LiveNodeType::SplitRgba(x) => x,
            LiveNodeType::MergeRgba(x) => x,
            LiveNodeType::Resize(x) => x,
        }
    }
    pub(crate) fn slots(&self, side: Side) -> Vec<Slot> {
        self.get_definition().slots(side)
    }

    pub(crate) fn process(&self, thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        self.get_definition().process(thread_pool, process_data);
    }
}

fn pixel_buffer(value: VismutPixel) -> Buffer {
    Buffer::from_raw(1, 1, vec![value]).unwrap()
}
