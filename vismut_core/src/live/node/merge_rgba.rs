use std::sync::Mutex;

use threadpool::ThreadPool;

use crate::address::{Side, SlotId};
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::slot::{Slot, SlotType};

use super::{LiveNodeDefinition, LiveNodeType};

#[derive(Debug, PartialEq, Eq)]
pub struct MergeRgbaLiveNode;
impl From<MergeRgbaLiveNode> for LiveNodeType {
    fn from(x: MergeRgbaLiveNode) -> Self {
        LiveNodeType::MergeRgba(x)
    }
}
impl LiveNodeDefinition for MergeRgbaLiveNode {
    fn slots(&self, side: Side) -> Vec<Slot> {
        if let Side::Input = side {
            vec![
                Slot::new(SlotType::Gray, SlotId(0), "red"),
                Slot::new(SlotType::Gray, SlotId(1), "green"),
                Slot::new(SlotType::Gray, SlotId(2), "blue"),
                Slot::new(SlotType::Gray, SlotId(3), "alpha"),
            ]
        } else {
            vec![Slot::new(SlotType::Rgba, SlotId(0), "output")]
        }
    }

    fn process(&self, _thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        process(process_data);
    }
}

pub(super) fn process(process_data: ProcessData) {
    let slot_images = process_data.get_slot_images(&[SlotId(0), SlotId(1), SlotId(2), SlotId(3)]);

    let output = vec![(
        SlotId(0),
        SlotImage::Rgba([
            slot_images[0]
                .expect("should always have all inputs")
                .inner_gray()
                .clone(),
            slot_images[1]
                .expect("should always have all inputs")
                .inner_gray()
                .clone(),
            slot_images[2]
                .expect("should always have all inputs")
                .inner_gray()
                .clone(),
            slot_images[3]
                .expect("should always have all inputs")
                .inner_gray()
                .clone(),
        ]),
    )];

    process_data.send(output).unwrap();
}
