use std::sync::Mutex;

use threadpool::ThreadPool;

use crate::address::{Side, SlotId};
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::pow_two::SizePow2;
use crate::slot::{Slot, SlotType};

use super::{LiveNodeDefinition, LiveNodeType};

#[derive(Debug, PartialEq, Eq)]
pub struct SplitRgbaLiveNode;
impl From<SplitRgbaLiveNode> for LiveNodeType {
    fn from(x: SplitRgbaLiveNode) -> Self {
        LiveNodeType::SplitRgba(x)
    }
}
impl LiveNodeDefinition for SplitRgbaLiveNode {
    fn slots(&self, side: Side) -> Vec<Slot> {
        if let Side::Input = side {
            vec![Slot::new(SlotType::Rgba, SlotId(0), "input")]
        } else {
            vec![
                Slot::new(SlotType::Gray, SlotId(0), "red"),
                Slot::new(SlotType::Gray, SlotId(1), "green"),
                Slot::new(SlotType::Gray, SlotId(2), "blue"),
                Slot::new(SlotType::Gray, SlotId(3), "alpha"),
            ]
        }
    }

    fn process(&self, _thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        process(process_data);
    }
}

pub(super) fn process(process_data: ProcessData) {
    let slot_image = process_data.get_slot_images(&[SlotId(0)])[0];

    let output = if let Some(slot_image) = slot_image {
        if let SlotImage::Rgba(buffers) = slot_image {
            vec![
                (SlotId(0), SlotImage::Gray(buffers[0].clone())),
                (SlotId(1), SlotImage::Gray(buffers[1].clone())),
                (SlotId(2), SlotImage::Gray(buffers[2].clone())),
                (SlotId(3), SlotImage::Gray(buffers[3].clone())),
            ]
        } else {
            panic!("should not be able to connect anything other than a SlotImage::Rgba here");
        }
    } else {
        vec![
            (
                SlotId(0),
                SlotImage::gray_from_value(SizePow2::default(), 0.0).unwrap(),
            ),
            (
                SlotId(1),
                SlotImage::gray_from_value(SizePow2::default(), 0.0).unwrap(),
            ),
            (
                SlotId(2),
                SlotImage::gray_from_value(SizePow2::default(), 0.0).unwrap(),
            ),
            (
                SlotId(3),
                SlotImage::gray_from_value(SizePow2::default(), 1.0).unwrap(),
            ),
        ]
    };

    process_data.send(output).unwrap();
}
