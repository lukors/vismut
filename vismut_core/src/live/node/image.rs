use super::{pixel_buffer, LiveNodeDefinition, LiveNodeType};

use crate::address::SlotId;
use crate::engine::VismutPixel;
use crate::error::{Result, VismutError};
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::pow_two::closest_pow_2;
use crate::prelude::Buffer;
use crate::slot::{Slot, SlotType};
use image::imageops::FilterType;
use image::{DynamicImage, ImageBuffer};
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex};
use threadpool::ThreadPool;

use crate::address::Side;

#[derive(Debug, PartialEq, Eq)]
pub struct ImageLiveNode(pub PathBuf);

impl From<ImageLiveNode> for LiveNodeType {
    fn from(x: ImageLiveNode) -> Self {
        LiveNodeType::Image(x)
    }
}

impl LiveNodeDefinition for ImageLiveNode {
    fn slots(&self, side: Side) -> Vec<Slot> {
        if let Side::Input = side {
            Vec::new()
        } else {
            vec![Slot::new(SlotType::Rgba, SlotId(0), "output")]
        }
    }

    fn process(&self, thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        process(process_data, thread_pool, self.0.clone());
    }
}

pub(super) fn process(process_data: ProcessData, thread_pool: &Mutex<ThreadPool>, path: PathBuf) {
    let thread_pool = thread_pool.lock().unwrap();
    thread_pool.execute(move || {
        let slot_image = match read_slot_image(path) {
            Ok(slot_image) => slot_image,
            Err(_) => SlotImage::Rgba([
                Arc::new(pixel_buffer(1.0)),
                Arc::new(pixel_buffer(0.0)),
                Arc::new(pixel_buffer(1.0)),
                Arc::new(pixel_buffer(1.0)),
            ]),
        };

        process_data.send(vec![(SlotId(0), slot_image)]).unwrap();
    });
}

fn read_slot_image<P: AsRef<Path>>(path: P) -> Result<SlotImage> {
    fn pop_buffer_to_arc(
        width: u32,
        height: u32,
        buffers: &mut Vec<Buffer>,
        default: f32,
    ) -> Arc<Buffer> {
        Arc::new(
            buffers
                .pop()
                .or_else(|| {
                    ImageBuffer::from_raw(width, height, vec![default; (width * height) as usize])
                })
                .unwrap(),
        )
    }

    let image = ::image::open(path)?;
    let width = closest_pow_2(image.width());
    let height = closest_pow_2(image.height());
    let image = image.resize_exact(width, height, FilterType::Triangle);

    let mut buffers = deconstruct_image(&image);

    match buffers.len() {
        1 => Ok(SlotImage::Gray(Arc::new(buffers.pop().unwrap()))),
        4 => {
            let (a, b, g, r) = (
                pop_buffer_to_arc(width, height, &mut buffers, 0.0),
                pop_buffer_to_arc(width, height, &mut buffers, 0.0),
                pop_buffer_to_arc(width, height, &mut buffers, 0.0),
                pop_buffer_to_arc(width, height, &mut buffers, 1.0),
            );
            Ok(SlotImage::Rgba([r, g, b, a]))
        }
        _ => Err(VismutError::InvalidBufferCount {
            actual: 0,
            expected: "1 or 4".to_string(),
        }),
    }
}

fn deconstruct_image(image: &DynamicImage) -> Vec<Buffer> {
    let pixels = image.as_flat_samples_u8().unwrap().samples;
    let (width, height) = (image.width(), image.height());
    let pixel_count = (width * height) as usize;
    let channel_count = pixels.len() / pixel_count;
    let max_channel_count = 4;
    let mut pixel_vectors: Vec<Vec<f32>> = Vec::with_capacity(max_channel_count);

    for _ in 0..max_channel_count {
        pixel_vectors.push(Vec::with_capacity(pixel_count));
    }

    let mut current_channel = 0;

    for component in pixels {
        pixel_vectors[current_channel].push(VismutPixel::from(*component) / 255.);
        current_channel = (current_channel + 1) % channel_count;
    }

    for (i, item) in pixel_vectors
        .iter_mut()
        .enumerate()
        .take(max_channel_count)
        .skip(channel_count)
    {
        *item = match i {
            3 => vec![1.; pixel_count],
            _ => vec![0.; pixel_count],
        }
    }

    pixel_vectors
        .into_iter()
        .map(|p_vec| {
            ImageBuffer::from_raw(width, height, p_vec)
                .expect("A bug in the deconstruct_image function caused a crash")
        })
        .collect()
}
