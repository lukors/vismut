use threadpool::ThreadPool;

use super::{pixel_buffer, LiveNodeDefinition, LiveNodeType};
use crate::address::{Side, SlotId};
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::slot::{Slot, SlotType};
use std::sync::{Arc, Mutex};

#[derive(Debug, PartialEq)]
pub struct GrayscaleLiveNode(pub f32);
impl From<GrayscaleLiveNode> for LiveNodeType {
    fn from(x: GrayscaleLiveNode) -> Self {
        LiveNodeType::Grayscale(x)
    }
}
impl LiveNodeDefinition for GrayscaleLiveNode {
    fn slots(&self, side: Side) -> Vec<Slot> {
        if let Side::Input = side {
            vec![Slot::new(SlotType::Gray, SlotId(0), "input")]
        } else {
            vec![Slot::new(SlotType::Gray, SlotId(0), "output")]
        }
    }

    fn process(&self, _thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        process(process_data, self.0)
    }
}

pub(super) fn process(process_data: ProcessData, value: f32) {
    let slot_image = process_data.get_slot_images(&[SlotId(0)])[0];

    let output = if let Some(slot_image) = slot_image {
        slot_image.clone()
    } else {
        SlotImage::Gray(Arc::new(pixel_buffer(value)))
    };

    process_data.send(vec![(SlotId(0), output)]).unwrap();
}
