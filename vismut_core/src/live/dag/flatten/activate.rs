use crate::address::{NodeAddress, Side, SlotId};
use crate::engine::NodeState;
use crate::live::address::LiveNodeId;
use crate::live::dag::LiveDag;
use crate::live::node::LiveNode;
use crate::live::node::LiveNodeType;

impl LiveDag {
    pub(crate) fn insert(
        &mut self,
        creator_address: NodeAddress,
        live_node_type: LiveNodeType,
    ) -> LiveNodeId {
        let live_node_id = self.new_id();

        let live_node = LiveNode {
            creator_address,
            live_node_id,
            live_node_type,
        };
        self.live_nodes.push(live_node);

        self.node_states.push((live_node_id, NodeState::Dirty));

        live_node_id
    }

    pub(crate) fn map_corresponding(
        &mut self,
        node_address: NodeAddress,
        live_node_id: LiveNodeId,
        side: Side,
        slot_id: SlotId,
    ) {
        self.map_different(node_address, live_node_id, side, slot_id, slot_id);
    }

    fn map_different(
        &mut self,
        node_address: NodeAddress,
        live_node_id: LiveNodeId,
        side: Side,
        blueprint_slot_id: SlotId,
        live_slot_id: SlotId,
    ) {
        let slot_address_side = node_address.with_slot_id(blueprint_slot_id).with_side(side);
        let live_slot_address_side = live_node_id.with_slot_id(live_slot_id).with_side(side);
        if let Some(_removed) = self
            .slot_address_map
            .insert(slot_address_side, live_slot_address_side)
        {
            panic!("added a slot mapping that already existed");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::address::{DagId, NodeId, SlotAddressSide};
    use crate::live::address::LiveSlotAddressSide;
    use crate::live::edge::LiveEdge;
    use crate::live::node::MergeRgbaLiveNode;
    use crate::live::node::ResizeLiveNode;
    use crate::live::node::SplitRgbaLiveNode;
    use crate::node_definition::NodeDefinition;
    use crate::pow_two::{Pow2, SizePow2};
    use crate::prelude::Resize;
    use crate::resize::{ResizeFilter, ResizePolicy};
    use crate::{MergeRgbaNode, SplitRgbaNode};
    use std::collections::BTreeMap;

    #[test]
    fn activate_merge_rgba() {
        // Preparations
        let mut live_dag = LiveDag::new();
        let creator_address = NodeAddress {
            dag_id: DagId(0),
            node_id: NodeId(0),
        };
        let absolute_size = SizePow2::new(Pow2::new(1), Pow2::new(1));
        let resize = Resize::new(ResizePolicy::Absolute(absolute_size), ResizeFilter::Nearest);

        let merge = MergeRgbaNode(resize);
        merge.activate(&mut live_dag, creator_address);

        // Assert LiveNodes
        let expected_live_nodes = vec![
            LiveNode {
                creator_address,
                live_node_id: LiveNodeId(0),
                live_node_type: ResizeLiveNode {
                    count: 4,
                    resize,
                    default: vec![0.0, 0.0, 0.0, 1.0],
                }
                .into(),
            },
            LiveNode {
                creator_address,
                live_node_id: LiveNodeId(1),
                live_node_type: MergeRgbaLiveNode.into(),
            },
        ];
        assert_eq!(
            live_dag.live_nodes, expected_live_nodes,
            "live nodes did not match expectations"
        );

        // Assert LiveEdges
        let live_node_resize = LiveNodeId(0);
        let live_node_merge_rgba = LiveNodeId(1);
        let expected_live_edges = vec![
            // Connections from Resize node to MergeRgba node.
            LiveEdge {
                output: live_node_resize.with_slot_id(SlotId(0)),
                input: live_node_merge_rgba.with_slot_id(SlotId(0)),
            },
            LiveEdge {
                output: live_node_resize.with_slot_id(SlotId(1)),
                input: live_node_merge_rgba.with_slot_id(SlotId(1)),
            },
            LiveEdge {
                output: live_node_resize.with_slot_id(SlotId(2)),
                input: live_node_merge_rgba.with_slot_id(SlotId(2)),
            },
            LiveEdge {
                output: live_node_resize.with_slot_id(SlotId(3)),
                input: live_node_merge_rgba.with_slot_id(SlotId(3)),
            },
        ];
        assert_eq!(
            live_dag.live_edges, expected_live_edges,
            "live edges did not match expectations"
        );

        // Assert mappings
        let mut expected_slot_address_map: BTreeMap<SlotAddressSide, LiveSlotAddressSide> =
            BTreeMap::new();
        let blueprint_node_address = NodeAddress {
            dag_id: DagId(0),
            node_id: NodeId(0),
        };

        // Resize node
        expected_slot_address_map.insert(
            blueprint_node_address
                .with_slot_id(SlotId(0))
                .with_side(Side::Input),
            live_node_resize
                .with_slot_id(SlotId(0))
                .with_side(Side::Input),
        );
        expected_slot_address_map.insert(
            blueprint_node_address
                .with_slot_id(SlotId(1))
                .with_side(Side::Input),
            live_node_resize
                .with_slot_id(SlotId(1))
                .with_side(Side::Input),
        );
        expected_slot_address_map.insert(
            blueprint_node_address
                .with_slot_id(SlotId(2))
                .with_side(Side::Input),
            live_node_resize
                .with_slot_id(SlotId(2))
                .with_side(Side::Input),
        );
        expected_slot_address_map.insert(
            blueprint_node_address
                .with_slot_id(SlotId(3))
                .with_side(Side::Input),
            live_node_resize
                .with_slot_id(SlotId(3))
                .with_side(Side::Input),
        );

        // MergeRgba node
        expected_slot_address_map.insert(
            blueprint_node_address
                .with_slot_id(SlotId(0))
                .with_side(Side::Output),
            live_node_merge_rgba
                .with_slot_id(SlotId(0))
                .with_side(Side::Output),
        );

        assert_eq!(
            live_dag.slot_address_map, expected_slot_address_map,
            "slot address mappings did not match expectations"
        );
    }

    #[test]
    fn activate_split_rgba() {
        // Preparations
        let mut live_dag = LiveDag::new();
        let creator_address = NodeAddress {
            dag_id: DagId(0),
            node_id: NodeId(0),
        };
        let absolute_size = SizePow2::new(Pow2::new(1), Pow2::new(1));
        let resize = Resize::new(ResizePolicy::Absolute(absolute_size), ResizeFilter::Nearest);

        let split = SplitRgbaNode(resize);
        split.activate(&mut live_dag, creator_address);

        // Assert LiveNodes
        let expected_live_nodes = vec![
            LiveNode {
                creator_address,
                live_node_id: LiveNodeId(0),
                live_node_type: ResizeLiveNode {
                    count: 4,
                    resize,
                    default: vec![0.0, 0.0, 0.0, 1.0],
                }
                .into(),
            },
            LiveNode {
                creator_address,
                live_node_id: LiveNodeId(1),
                live_node_type: SplitRgbaLiveNode.into(),
            },
        ];
        assert_eq!(
            live_dag.live_nodes, expected_live_nodes,
            "live nodes did not match expectations"
        );

        // Assert LiveEdges
        let live_node_resize = LiveNodeId(0);
        let live_node_split_rgba = LiveNodeId(1);
        let expected_live_edges = vec![
            // Connections from SplitRgba node to Resize node.
            LiveEdge {
                output: live_node_split_rgba.with_slot_id(SlotId(0)),
                input: live_node_resize.with_slot_id(SlotId(0)),
            },
            LiveEdge {
                output: live_node_split_rgba.with_slot_id(SlotId(1)),
                input: live_node_resize.with_slot_id(SlotId(1)),
            },
            LiveEdge {
                output: live_node_split_rgba.with_slot_id(SlotId(2)),
                input: live_node_resize.with_slot_id(SlotId(2)),
            },
            LiveEdge {
                output: live_node_split_rgba.with_slot_id(SlotId(3)),
                input: live_node_resize.with_slot_id(SlotId(3)),
            },
        ];
        assert_eq!(
            live_dag.live_edges, expected_live_edges,
            "live edges did not match expectations"
        );

        // Assert mappings
        let mut expected_slot_address_map: BTreeMap<SlotAddressSide, LiveSlotAddressSide> =
            BTreeMap::new();
        let blueprint_node_address = NodeAddress {
            dag_id: DagId(0),
            node_id: NodeId(0),
        };

        // SplitRgba node
        expected_slot_address_map.insert(
            blueprint_node_address
                .with_slot_id(SlotId(0))
                .with_side(Side::Input),
            live_node_split_rgba
                .with_slot_id(SlotId(0))
                .with_side(Side::Input),
        );

        // Resize node
        expected_slot_address_map.insert(
            blueprint_node_address
                .with_slot_id(SlotId(0))
                .with_side(Side::Output),
            live_node_resize
                .with_slot_id(SlotId(0))
                .with_side(Side::Output),
        );
        expected_slot_address_map.insert(
            blueprint_node_address
                .with_slot_id(SlotId(1))
                .with_side(Side::Output),
            live_node_resize
                .with_slot_id(SlotId(1))
                .with_side(Side::Output),
        );
        expected_slot_address_map.insert(
            blueprint_node_address
                .with_slot_id(SlotId(2))
                .with_side(Side::Output),
            live_node_resize
                .with_slot_id(SlotId(2))
                .with_side(Side::Output),
        );
        expected_slot_address_map.insert(
            blueprint_node_address
                .with_slot_id(SlotId(3))
                .with_side(Side::Output),
            live_node_resize
                .with_slot_id(SlotId(3))
                .with_side(Side::Output),
        );

        assert_eq!(
            live_dag.slot_address_map, expected_slot_address_map,
            "slot address mappings did not match expectations"
        );
    }
}
