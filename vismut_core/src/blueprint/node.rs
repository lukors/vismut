use serde::{Deserialize, Serialize};

use crate::address::NodeId;
use crate::node::NodeType;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub(crate) struct BlueprintNode {
    pub node_id: NodeId,
    pub node_type: NodeType,
}

impl BlueprintNode {
    pub const fn new(node_id: NodeId, node: NodeType) -> Self {
        Self {
            node_id,
            node_type: node,
        }
    }
}
