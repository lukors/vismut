use serde::{Deserialize, Serialize};

use crate::blueprint::dag::BlueprintDag;
use crate::blueprint::node::BlueprintNode;
use crate::edge::DagEdge;
use crate::error::VismutError::InvalidNodeId;
use crate::error::{Result, VismutError};
use crate::prelude::*;

/// The public interface for `Dag`. This can be created and set up however the user wants outside
/// of an `Engine`, and when it's inserted into an `Engine` it is transformed into a
/// `BlueprintDag`.
#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct Dag {
    pub(crate) node_types: Vec<BlueprintNode>,
    pub(crate) edges: Vec<DagEdge>,
    #[serde(skip)]
    pub(crate) node_counter: usize,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SerializedPosition {
    pub node_id: NodeId,
    pub x: i32,
    pub y: i32,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SerializeableDag {
    pub dag: Dag,
    pub positions: Vec<SerializedPosition>,
}

impl Default for SerializeableDag {
    fn default() -> Self {
        Self::new()
    }
}

impl SerializeableDag {
    pub fn new() -> Self {
        Self {
            dag: Dag::new(),
            positions: Vec::new(),
        }
    }
}

impl Dag {
    pub const fn new() -> Self {
        Self {
            node_types: Vec::new(),
            edges: Vec::new(),
            node_counter: 0,
        }
    }

    pub(crate) fn into_blueprint_dag(self, dag_id: DagId, one_shot: bool) -> BlueprintDag {
        BlueprintDag::new(dag_id, self, one_shot)
    }

    /// Returns an iterator over all `NodeId`s in the `Dag`.
    pub fn node_id_iter(&self) -> impl Iterator<Item = NodeId> + '_ {
        self.node_types
            .iter()
            .map(|blueprint_node| blueprint_node.node_id)
    }

    /// Returns an iterator over all `NodeId`s and their corresponding `NodeType`s.
    pub fn node_type_iter(&self) -> impl Iterator<Item = (NodeId, &NodeType)> + '_ {
        self.node_types
            .iter()
            .map(|blueprint_node| (blueprint_node.node_id, &blueprint_node.node_type))
    }

    /// Iterates over all the `DagEdge`s in the `Dag`.
    pub fn edges_iter(&self) -> impl Iterator<Item = &DagEdge> + '_ {
        self.edges.iter()
    }

    /// Inserts a `NodeType` into the `Dag`, returning the `NodeId` that was created for it.
    pub fn insert(&mut self, node_type: NodeType) -> NodeId {
        let node_type = self.avoid_new_name_collision(node_type);
        let node_id = self.new_id();
        let blueprint_node = BlueprintNode::new(node_id, node_type);

        self.node_types.push(blueprint_node);

        node_id
    }

    /// Inserts a `NodeType` into the `Dag` with the given `NodeId`.
    ///
    /// Returns an error if the `NodeId` was already taken, and then the `NodeType` is not inserted.
    pub fn insert_with_id(&mut self, node_type: NodeType, node_id: NodeId) -> Result<()> {
        let node_type = self.avoid_new_name_collision(node_type);

        if self.node_type(node_id).is_ok() {
            Err(VismutError::InvalidNodeId(node_id))
        } else {
            let blueprint_node = BlueprintNode::new(node_id, node_type);
            self.node_types.push(blueprint_node);
            Ok(())
        }
    }

    /// If `node_type` has a name, make sure it doesn't collide with any of the other names in the
    /// `Dag`. This is used with nodes that haven't been added to the `Dag` yet.
    fn avoid_new_name_collision(&self, node_type: NodeType) -> NodeType {
        let mut node_type = node_type;

        if let Ok(name) = node_type.name() {
            let new_name = self.avoid_name_collision(None, name.clone());
            node_type
                .set_name(new_name)
                .expect("since we just got the name, we should be able to set it");
        };

        node_type
    }

    /// Removes and returns the node with the given `NodeId`.
    pub fn remove(&mut self, node_id: NodeId) -> Result<NodeType> {
        // Remove nodes
        let node_index = self
            .node_types
            .iter()
            .enumerate()
            .find(|(_, node)| node.node_id == node_id)
            .map(|(index, _)| index)
            .ok_or(VismutError::InvalidNodeId(node_id))?;
        let blueprint_node = self.node_types.swap_remove(node_index);

        // Remove connected edges
        #[allow(clippy::needless_collect)]
        let edge_indices = self
            .edges
            .iter()
            .enumerate()
            .filter(|(_, edge)| edge.node_id_input == node_id || edge.node_id_output == node_id)
            .map(|(index, _)| index)
            .collect::<Vec<_>>();
        for index in edge_indices.into_iter().rev() {
            self.edges.swap_remove(index);
        }

        Ok(blueprint_node.node_type)
    }

    // todo: maybe make this private, if we can deal without this in the GUI
    /// Returns a new `NodeId`, which has not been used yet.
    ///
    /// Note: A `NodeId` is only guaranteed to be unique when it's returned by this function.
    /// The `NodeId` needs to be used for a `NodeType` on the `Dag`,
    /// with for instance `insert_with_id`,
    /// to guarantee that another `NodeType` does not use the `NodeId`.
    pub fn new_id(&mut self) -> NodeId {
        let mut id = self.node_counter;
        self.node_counter += 1;

        while self.node_type(NodeId(id)).is_ok() {
            id = self.node_counter;
            self.node_counter += 1;
        }

        NodeId(id)
    }

    /// Force an edge to be created if possible, removing any existing edges that might hinder
    /// the connection. Except if the exact edge already exists, then an error is returned.
    pub fn connect(
        &mut self,
        node_id_output: NodeId,
        slot_id_output: SlotId,
        node_id_input: NodeId,
        slot_id_input: SlotId,
    ) -> Result<()> {
        let edge = DagEdge::new(node_id_output, slot_id_output, node_id_input, slot_id_input);
        if self.edges.contains(&edge) {
            return Err(VismutError::EdgeAlreadyExists(edge));
        }

        self.connection_valid(edge)?;
        self.disconnect_slot(node_id_input, Side::Input, slot_id_input)?;
        self.edges.push(edge);

        Ok(())
    }

    pub(crate) fn connection_valid(&self, edge: DagEdge) -> Result<()> {
        if edge.node_id_input == edge.node_id_output {
            return Err(VismutError::InvalidEdge);
        }

        let input_slot = self
            .node_type(edge.node_id_input)?
            .slot_from_id(Side::Input, edge.slot_id_input)?;
        let output_slot = self
            .node_type(edge.node_id_output)?
            .slot_from_id(Side::Output, edge.slot_id_output)?;

        input_slot.slot_type.compatible(output_slot.slot_type)?;

        if self.edges.iter().any(|i_edge| {
            edge.node_id_input == i_edge.node_id_input && edge.slot_id_input == i_edge.slot_id_input
        }) {
            Err(VismutError::SlotOccupied(edge.slot_id_input))
        } else {
            Ok(())
        }
    }

    pub fn node_type(&self, node_id: NodeId) -> Result<&NodeType> {
        if let Some(blueprint_node) = self
            .node_types
            .iter()
            .find(|blueprint_node| blueprint_node.node_id == node_id)
        {
            Ok(&blueprint_node.node_type)
        } else {
            Err(VismutError::InvalidNodeId(node_id))
        }
    }

    pub fn node_type_mut(&mut self, node_id: NodeId) -> Result<&mut NodeType> {
        if let Some(blueprint_node) = self
            .node_types
            .iter_mut()
            .find(|blueprint_node| blueprint_node.node_id == node_id)
        {
            Ok(&mut blueprint_node.node_type)
        } else {
            Err(VismutError::InvalidNodeId(node_id))
        }
    }

    fn disconnect_slot(&mut self, node_id: NodeId, side: Side, slot_id: SlotId) -> Result<()> {
        if let Ok(node_type) = self.node_type(node_id) {
            node_type.slot_id_exists(side, slot_id)?;
        } else {
            return Err(InvalidNodeId(node_id));
        }

        #[allow(clippy::needless_collect)]
        let edge_indices = self
            .edges
            .iter()
            .enumerate()
            .filter(|(_i, edge)| {
                if let Side::Input = side {
                    edge.node_id_input == node_id && edge.slot_id_input == slot_id
                } else {
                    edge.node_id_output == node_id && edge.slot_id_output == slot_id
                }
            })
            .map(|(i, _edge)| i)
            .collect::<Vec<_>>();

        for index in edge_indices.into_iter().rev() {
            self.edges.remove(index);
        }

        Ok(())
    }

    pub(crate) fn remove_edge(&mut self, blueprint_edge: DagEdge) -> Result<()> {
        let index = self
            .edges
            .iter()
            .enumerate()
            .find(|(_, edge)| **edge == blueprint_edge)
            .ok_or(VismutError::InvalidEdge)
            .map(|(index, _)| index)?;

        self.edges.remove(index);
        Ok(())
    }

    pub(crate) fn output_addresses(&self) -> Vec<(NodeId, SlotId)> {
        self.node_types
            .iter()
            .filter(|node| {
                node.node_type
                    .eq_discriminant(&NodeType::OutputRgba(OutputRgbaNode(String::new())))
            })
            .map(|node| (node.node_id, SlotId(0)))
            .collect()
    }

    pub fn set_name(&mut self, node_id: NodeId, name: String) -> Result<()> {
        let new_name = self.avoid_name_collision(Some(node_id), name);
        self.node_type_mut(node_id)?.set_name(new_name)
    }

    fn avoid_name_collision(&self, ignore_node_id: Option<NodeId>, name: String) -> String {
        let mut new_name = name;

        let other_names = self
            .node_types
            .iter()
            .filter(|blueprint_node| {
                if let Some(ignore_node_id) = ignore_node_id {
                    blueprint_node.node_id != ignore_node_id
                } else {
                    true
                }
            })
            .filter_map(|blueprint_node| blueprint_node.node_type.name().ok());

        while other_names.clone().any(|name| name == &new_name) {
            if let Some((name, number)) = new_name.rsplit_once('_') {
                if number.chars().all(char::is_numeric) {
                    let number = if let Ok(number) = number.parse::<u32>() {
                        number.wrapping_add(1)
                    } else {
                        0
                    };

                    new_name = String::from(format!("{}_{}", name, number).as_str());
                } else {
                    new_name = String::from(format!("{}_0", name).as_str());
                }
            } else {
                new_name = String::from(format!("{}_0", new_name).as_str());
            }
        }

        new_name
    }
}
