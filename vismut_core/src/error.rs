use crate::address::{NodeAddress, SlotAddress};
use crate::edge::DagEdge;
use crate::prelude::*;
use crate::slot::SlotType;
use std::fmt::Debug;
use std::sync::mpsc;
use thiserror::Error;

pub type Result<T> = std::result::Result<T, VismutError>;

#[derive(Error, Debug)]
pub enum VismutError {
    #[error("could not find a dag with id `{0}`")]
    InvalidDagId(DagId),
    #[error("could not find a node with id `{0}`")]
    InvalidNodeId(NodeId),
    #[error("the side was not valid")]
    InvalidSide,
    #[error("could not find a slot with id `{0}`")]
    InvalidSlotId(SlotId),
    #[error("could not find a node with address `{0}`")]
    InvalidNodeAddress(NodeAddress),
    #[error("could not find a sideless slot at `{0}`")]
    InvalidSlotAddress(SlotAddress),
    #[error("could not find a slot on the {1} side of `{0}`")]
    InvalidSlotAddressSide(SlotAddress, Side),
    #[error("incompatible slot types: `{0}` and `{1}`")]
    IncompatibleSlotTypes(SlotType, SlotType),
    #[error("edge already exists: `{0}`")]
    EdgeAlreadyExists(DagEdge),
    #[error("tried creating an invalid edge")]
    InvalidEdge,
    #[error("the slot is already occupied: `{0}`")]
    SlotOccupied(SlotId),
    #[error("invalid buffer count, actual: `{actual}`, expected: `{expected}`")]
    InvalidBufferCount { actual: usize, expected: String },
    #[error("{0}")]
    Image(image::ImageError),
    #[error("node has wrong type `{0:?}`")]
    IncompatibleNodeType(String),
    #[error("could not find an edge connected to `({dag_id}, {node_id}, {side}, {slot_id})`")]
    NoEdge {
        dag_id: DagId,
        node_id: NodeId,
        side: Side,
        slot_id: SlotId,
    },
    #[error("the sender's receiver has been destroyed")]
    SendError,
}

impl From<image::ImageError> for VismutError {
    fn from(cause: image::ImageError) -> VismutError {
        Self::Image(cause)
    }
}

// Todo: This doesn't seem to be usable?
impl<T> From<mpsc::SendError<T>> for VismutError {
    fn from(_payload: mpsc::SendError<T>) -> VismutError {
        Self::SendError
    }
}
