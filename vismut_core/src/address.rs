use std::fmt::{Display, Formatter};

use serde::{Deserialize, Serialize};

/// The ID of a dag in the context of an `Engine`.
#[derive(
    Copy, Clone, Default, Debug, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize, Hash,
)]
pub struct DagId(pub(crate) usize);

impl Display for DagId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl DagId {
    pub fn inner(&self) -> usize {
        self.0
    }

    pub fn with_node_id(&self, node_id: NodeId) -> NodeAddress {
        NodeAddress {
            dag_id: *self,
            node_id,
        }
    }
}

/// The ID of a node in the context of a dag.
#[derive(
    Copy, Clone, Debug, Default, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize, Hash,
)]
pub struct NodeId(pub usize);

impl Display for NodeId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Refers to a certain side of a node.
#[derive(Copy, Clone, Debug, Eq, PartialEq, PartialOrd, Ord)]
pub enum Side {
    Input,
    Output,
}

impl Default for Side {
    fn default() -> Self {
        Self::Input
    }
}

impl Display for Side {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let text = if let Self::Input = self {
            "input"
        } else {
            "output"
        };

        write!(f, "{}", text)
    }
}

/// The ID of a slot in the context of a side on a node.
#[derive(Copy, Clone, Debug, Default, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub struct SlotId(pub usize);

impl Display for SlotId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Globally refers to a specific node.
#[derive(
    Copy, Clone, Debug, Default, Ord, PartialOrd, Eq, PartialEq, Hash, Serialize, Deserialize,
)]
pub struct NodeAddress {
    pub(crate) dag_id: DagId,
    pub(crate) node_id: NodeId,
}

impl Display for NodeAddress {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "(DagId: {}, NodeId: {})", self.dag_id, self.node_id)
    }
}

impl NodeAddress {
    pub fn new(dag_id: DagId, node_id: NodeId) -> Self {
        Self { dag_id, node_id }
    }

    pub fn with_slot_id(&self, slot_id: SlotId) -> SlotAddress {
        SlotAddress {
            dag_id: self.dag_id,
            node_id: self.node_id,
            slot_id,
        }
    }

    pub fn without_dag_id(&self) -> DagId {
        self.dag_id
    }

    pub fn to_tuple(self) -> (DagId, NodeId) {
        (self.dag_id, self.node_id)
    }

    pub fn dag_id(&self) -> DagId {
        self.dag_id
    }

    pub fn node_id(&self) -> NodeId {
        self.node_id
    }
}

/// Globally refers to a specific slot.
#[derive(Clone, Copy, Debug, Default, Eq, PartialEq, Ord, PartialOrd)]
pub struct SlotAddressSide {
    pub dag_id: DagId,
    pub node_id: NodeId,
    pub side: Side,
    pub slot_id: SlotId,
}

impl Display for SlotAddressSide {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "(DagId: {}, NodeId: {}, Side: {}, SlotId: {})",
            self.dag_id, self.node_id, self.side, self.slot_id
        )
    }
}

impl SlotAddressSide {
    pub const fn without_side(self) -> SlotAddress {
        SlotAddress {
            dag_id: self.dag_id,
            node_id: self.node_id,
            slot_id: self.slot_id,
        }
    }

    pub fn node_address(self) -> NodeAddress {
        NodeAddress {
            dag_id: self.dag_id,
            node_id: self.node_id,
        }
    }
}

/// Used to refer to a slot in situations where which side the slot is on is implied by the usage.
/// For instance, the slot occupied by a buffer is always going to be on the output side, since an
/// input slot can't "have" a buffer.
#[derive(Copy, Clone, Debug, Default, Eq, PartialEq)]
pub struct SlotAddress {
    pub(crate) dag_id: DagId,
    pub(crate) node_id: NodeId,
    pub(crate) slot_id: SlotId,
}

impl Display for SlotAddress {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "(DagId({}), NodeId({}), SlotId({})",
            self.dag_id, self.node_id, self.slot_id
        )
    }
}

impl SlotAddress {
    pub fn new(dag_id: DagId, node_id: NodeId, slot_id: SlotId) -> Self {
        Self {
            dag_id,
            node_id,
            slot_id,
        }
    }

    pub fn to_tuple(self) -> (DagId, NodeId, SlotId) {
        (self.dag_id, self.node_id, self.slot_id)
    }

    pub fn dag_id(&self) -> DagId {
        self.dag_id
    }
    pub fn node_id(&self) -> NodeId {
        self.node_id
    }
    pub fn slot_id(&self) -> SlotId {
        self.slot_id
    }

    pub fn with_side(&self, side: Side) -> SlotAddressSide {
        SlotAddressSide {
            dag_id: self.dag_id,
            node_id: self.node_id,
            side,
            slot_id: self.slot_id,
        }
    }

    pub fn without_slot_id(&self) -> NodeAddress {
        NodeAddress {
            dag_id: self.dag_id,
            node_id: self.node_id,
        }
    }
}
