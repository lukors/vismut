use std::fmt::Debug;

use crate::error::Result;
use crate::prelude::Resize;
use crate::{
    error::VismutError,
    live::dag::LiveDag,
    prelude::{NodeAddress, Side, Slot, SlotId},
};

pub(crate) trait NodeDefinition: Debug {
    fn slots(&self, side: Side) -> Vec<Slot>;
    fn slot_from_id(&self, side: Side, slot_id: SlotId) -> Result<Slot> {
        self.slots(side)
            .iter()
            .find(|&s| s.slot_id == slot_id)
            .map_or(Err(VismutError::InvalidSlotId(slot_id)), |x| Ok(x.clone()))
    }
    fn slot_id_exists(&self, side: Side, slot_id: SlotId) -> Result<()> {
        self.slots(side)
            .iter()
            .find(|&s| s.slot_id == slot_id)
            .map_or(Err(VismutError::InvalidSlotId(slot_id)), |_| Ok(()))
    }

    fn activate(&self, live_dag: &mut LiveDag, creator_address: NodeAddress);

    fn name(&self) -> Result<&String> {
        Err(VismutError::IncompatibleNodeType(format!("{:?}", self)))
    }
    fn set_name(&mut self, _new_name: String) -> Result<()> {
        Err(VismutError::IncompatibleNodeType(format!("{:?}", self)))
    }
    fn resize(&self) -> Option<Resize> {
        None
    }
    fn resize_mut(&mut self) -> Option<&mut Resize> {
        None
    }
    fn title(&self) -> String;
}
