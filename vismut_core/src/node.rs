use crate::error::Result;

use crate::node_definition::NodeDefinition;
use crate::prelude::*;
use crate::resize::Resize;
use crate::slot::Slot;
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

/// This enum is the public interface for nodes. It makes it friendly and easy to edit the node
/// dag. When `Engine.prepare()` is executed before the node dag gets processed, these nodes are
/// transformed into `LiveNodeType`s, which can actually be processed. One `NodeType` can be
/// transformed into several `LiveNodeType`s.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum NodeType {
    /// Reads the specified image from disk. If the image's size is not a power of two it will be
    /// resized to the closest power of two on each axis.
    Image(ImageNode),
    OutputRgba(OutputRgbaNode),
    /// Creates a new grayscale image containing one pixel of the given value.
    Grayscale(GrayscaleNode),
    /// Takes one rgba image and splits it into 4 grayscale images.
    SplitRgba(SplitRgbaNode),
    /// Takes 4 grayscale images and combines them into one rgba image.
    MergeRgba(MergeRgbaNode),
}

impl Display for NodeType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.get_definition().title())
    }
}

impl NodeType {
    pub(crate) fn get_definition(&self) -> &dyn NodeDefinition {
        match self {
            NodeType::Image(x) => x,
            NodeType::OutputRgba(x) => x,
            NodeType::Grayscale(x) => x,
            NodeType::SplitRgba(x) => x,
            NodeType::MergeRgba(x) => x,
        }
    }
    pub(crate) fn get_definition_mut(&mut self) -> &mut dyn NodeDefinition {
        match self {
            NodeType::Image(x) => x,
            NodeType::OutputRgba(x) => x,
            NodeType::Grayscale(x) => x,
            NodeType::SplitRgba(x) => x,
            NodeType::MergeRgba(x) => x,
        }
    }
    pub fn name(&self) -> Result<&String> {
        self.get_definition().name()
    }

    /// If the `NodeType` has a name, this function sets it to the input `String`.
    pub(crate) fn set_name(&mut self, new_name: String) -> Result<()> {
        self.get_definition_mut().set_name(new_name)
    }

    pub fn slot_id_exists(&self, side: Side, slot_id: SlotId) -> Result<()> {
        self.get_definition().slot_id_exists(side, slot_id)
    }

    pub fn eq_discriminant(&self, other: &Self) -> bool {
        std::mem::discriminant(self) == std::mem::discriminant(other)
    }

    pub fn slots(&self, side: Side) -> Vec<Slot> {
        self.get_definition().slots(side)
    }

    pub fn resize(&self) -> Option<Resize> {
        self.get_definition().resize()
    }

    pub fn resize_mut(&mut self) -> Option<&mut Resize> {
        self.get_definition_mut().resize_mut()
    }

    pub fn slot_from_id(&self, side: Side, slot_id: SlotId) -> Result<Slot> {
        self.get_definition().slot_from_id(side, slot_id)
    }
}
