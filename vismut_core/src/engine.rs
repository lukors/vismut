use crate::address::{DagId, NodeAddress, NodeId, SlotAddress};
use crate::blueprint::dag::BlueprintDag;
use crate::dag::Dag;
use crate::edge::DagEdge;
use crate::edge::Edge;
use crate::error::VismutError::InvalidDagId;
use crate::error::{Result, VismutError};
use crate::live::dag::LiveDag;
use crate::node::NodeType;
use crate::pow_two::SizePow2;
use std::collections::{BTreeMap, BTreeSet};
use std::num::NonZeroUsize;
use std::sync::{mpsc, Mutex};

#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum NodeState {
    Dirty,
    Processing,
    Clean,
}

impl Default for NodeState {
    fn default() -> Self {
        Self::Dirty
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
pub(crate) struct InterDagEdge {
    pub output: SlotAddress,
    pub input: SlotAddress,
}

impl From<Edge> for InterDagEdge {
    fn from(edge: Edge) -> Self {
        Self {
            output: edge.slot_address_output,
            input: edge.slot_address_input,
        }
    }
}

pub type VismutPixel = f32;

struct RequestedBuffer {
    slot_address: SlotAddress,
    sender: Mutex<mpsc::Sender<(SizePow2, Vec<u8>)>>,
}

pub struct Engine {
    /// These are the dags that `live_nodes` and `live_edges` get created from. Working with only
    /// nodes, without any dags, simplifies the node processing step.
    blueprint_dags: Vec<BlueprintDag>,
    /// The connections between nodes in dags.
    inter_dag_edges: Vec<InterDagEdge>,
    /// This is the single flat DAG that is used when processing.
    live_dag: LiveDag,
    /// The buffers that have been requested since the last run of "prepare".
    added_buffer_handles: Vec<RequestedBuffer>,
    /// Ensures a unique `DagId` for each DAG.
    dag_counter: usize,
}

impl Default for Engine {
    fn default() -> Self {
        Self::new()
    }
}

impl Engine {
    pub fn new() -> Self {
        Engine {
            blueprint_dags: Vec::new(),
            inter_dag_edges: Vec::new(),
            live_dag: LiveDag::new(),
            added_buffer_handles: Vec::new(),
            dag_counter: 0,
        }
    }

    /// Sets up the dag to make it ready to be executed. This needs to be called before calling
    /// `run()` for any changes to make a difference.
    pub fn prepare(&mut self) {
        self.clean_up_one_shot_dags();
        self.live_dag
            .prepare(&self.blueprint_dags, &self.inter_dag_edges);
        self.insert_added_buffer_handles();
    }

    /// Inserts all added buffer handles into the `LiveDag` to be awaited.
    fn insert_added_buffer_handles(&mut self) {
        while let Some(RequestedBuffer {
            slot_address,
            sender,
        }) = self.added_buffer_handles.pop()
        {
            let _ = self.live_dag.insert_waiting_sender(sender, slot_address);
        }
    }

    /// Starts processing any nodes that can be processed.
    pub fn run(&mut self) {
        self.live_dag.run();
    }

    /// Removes any one shot dags that are finished processing.
    fn clean_up_one_shot_dags(&mut self) {
        let unclean_addresses = self.live_dag.nodes_not_in_state(NodeState::Clean);

        let unclean_dag_ids = unclean_addresses
            .into_iter()
            .map(|node_address| node_address.dag_id)
            .collect::<BTreeSet<_>>();

        let one_shot_dag_ids = self
            .blueprint_dags
            .iter()
            .filter(|dag| dag.one_shot)
            .map(|dag| dag.dag_id)
            .collect::<Vec<_>>();

        for dag_id in one_shot_dag_ids {
            if !unclean_dag_ids.contains(&dag_id) {
                self.remove(dag_id).expect("the dag should exist");
            }
        }
    }

    /// Returns the state of each node in the given DAG.
    pub fn node_states(&self, dag_id: DagId) -> BTreeMap<NodeId, NodeState> {
        self.live_dag.node_states(dag_id)
    }

    /// Returns all `NodeId`s in the given `NodeState` and `DagId`. Is unsorted and may contain
    /// duplicates.
    pub fn nodes_in_state(&self, dag_id: DagId, node_state: NodeState) -> Vec<NodeId> {
        self.live_dag
            .nodes_in_state(node_state)
            .iter()
            .filter(|node_address| node_address.dag_id == dag_id)
            .map(|node_address| node_address.node_id)
            .collect()
    }

    /// Processes over and over until the entire dag is processed.
    pub fn run_until_done(&mut self) {
        self.live_dag.run_until_done();
    }

    /// Sets the number of threads to use for processing nodes. Note that reducing the count does
    /// not actually kill any threads, they will just sit unused.
    pub fn set_num_threads(&mut self, num_threads: NonZeroUsize) {
        self.live_dag.set_num_threads(num_threads);
    }

    /// Returns the buffer in the given `SlotAddressSideless` as a `Vec<u8>` so it can for instance
    /// be saved to a file on disk.
    pub fn buffer_rgba(&self, slot_address: SlotAddress) -> Result<Vec<u8>> {
        self.live_dag.buffer_rgba(slot_address)
    }

    pub fn buffer_rgba_receiver(
        &mut self,
        slot_address: SlotAddress,
    ) -> Result<mpsc::Receiver<(SizePow2, Vec<u8>)>> {
        let (sender, receiver) = mpsc::channel();
        let added_buffer_handle = RequestedBuffer {
            slot_address,
            sender: Mutex::new(sender),
        };
        self.added_buffer_handles.push(added_buffer_handle);

        Ok(receiver)
    }

    /// Returns the size of the image in the output slot at `slot_address`.
    pub fn slot_size(&self, slot_address: SlotAddress) -> Result<SizePow2> {
        self.live_dag.slot_size(slot_address)
    }

    /// Inserts a `Dag` and returns its unique `DagId`.
    pub fn insert(&mut self, dag: Dag) -> DagId {
        self.insert_internal(dag, false)
    }

    /// Inserts a "one shot" `Dag` and returns its unique `DagId`. A one shot dag gets automatically
    /// removed when it has finished processing.
    pub fn insert_one_shot(&mut self, dag: Dag) -> DagId {
        self.insert_internal(dag, true)
    }

    /// Inserts a `Dag` and returns its unique `DagId`. This function lets you pick if you want to
    /// the inserted `Dag` to be "one shot" or not.
    fn insert_internal(&mut self, dag: Dag, one_shot: bool) -> DagId {
        let dag_id = self.new_id();
        let blueprint_dag = dag.into_blueprint_dag(dag_id, one_shot);

        self.blueprint_dags.push(blueprint_dag);

        dag_id
    }

    /// Removes a `Dag` from the `Engine`.
    pub fn remove(&mut self, dag_id: DagId) -> Result<Dag> {
        if let Some(index) = self
            .blueprint_dags
            .iter()
            .position(|blueprint_dag| blueprint_dag.dag_id == dag_id)
        {
            // Remove any connections between this DAG and other DAGs.
            #[allow(clippy::needless_collect)]
            let inter_dag_edge_indices = self
                .inter_dag_edges
                .iter()
                .enumerate()
                .filter(|(.., edge)| edge.input.dag_id == dag_id || edge.output.dag_id == dag_id)
                .map(|(index, ..)| index)
                .collect::<Vec<_>>();

            for index in inter_dag_edge_indices.into_iter().rev() {
                self.inter_dag_edges.swap_remove(index);
            }

            // Remove the DAG itself.
            let blueprint_dag = self.blueprint_dags.swap_remove(index);
            Ok(blueprint_dag.dag)
        } else {
            Err(InvalidDagId(dag_id))
        }
    }

    /// Inserts a node into a dag if it existsS.
    pub fn insert_node(&mut self, dag_id: DagId, node_type: NodeType) -> Result<NodeAddress> {
        let node_id = self.blueprint_dag_mut(dag_id)?.insert(node_type);
        Ok(dag_id.with_node_id(node_id))
    }

    /// Inserts a node at the given address, fails if the address is already occupied.
    pub fn insert_node_with_address(
        &mut self,
        node_type: NodeType,
        node_address: NodeAddress,
    ) -> Result<()> {
        self.blueprint_dag_mut(node_address.dag_id)?
            .insert_with_id(node_type, node_address.node_id)?;
        Ok(())
    }

    /// Removes the `NodeType` at the given `NodeAddress`.
    pub fn remove_node(&mut self, node_address: NodeAddress) -> Result<NodeType> {
        // Find and remove edges between DAGs, if there are any.
        #[allow(clippy::needless_collect)]
        let inter_dag_edge_indices = self
            .inter_dag_edges
            .iter()
            .enumerate()
            .filter(|(.., inter_dag_edge)| {
                inter_dag_edge.output.without_slot_id() == node_address
                    || inter_dag_edge.input.without_slot_id() == node_address
            })
            .map(|(index, ..)| index)
            .collect::<Vec<_>>();

        for index in inter_dag_edge_indices.into_iter().rev() {
            self.inter_dag_edges.swap_remove(index);
        }

        // Remove the node itself.
        self.blueprint_dag_mut(node_address.dag_id)?
            .remove(node_address.node_id)
    }

    /// Removes an `Edge` that matches the input.
    pub fn remove_edge(&mut self, edge: Edge) -> Result<()> {
        if edge.slot_address_output.dag_id == edge.slot_address_input.dag_id {
            let blueprint_edge = DagEdge::from(edge);
            let dag = self.blueprint_dag_mut(edge.slot_address_input.dag_id)?;
            dag.remove_edge(blueprint_edge)
        } else {
            let inter_dag_edge = edge.into();

            if let Some(index) = self
                .inter_dag_edges
                .iter()
                .position(|edge| *edge == inter_dag_edge)
            {
                self.inter_dag_edges.swap_remove(index);
                Ok(())
            } else {
                Err(VismutError::InvalidEdge)
            }
        }
    }

    /// Returns a new address that can safely be used with `insert_node_with_address()`.
    pub fn new_address(&mut self, dag_id: DagId) -> Result<NodeAddress> {
        let node_id = self.blueprint_dag_mut(dag_id)?.new_id();
        Ok(dag_id.with_node_id(node_id))
    }

    /// Returns a reference to the `NodeType`` at the address, if it exists.
    pub fn node_type(&self, node_address: NodeAddress) -> Result<&NodeType> {
        let dag_id = node_address.dag_id;

        let dag = self
            .blueprint_dags
            .iter()
            .find(|dag| dag.dag_id == dag_id)
            .ok_or(VismutError::InvalidDagId(dag_id))?;

        let node_type = dag.dag.node_type(node_address.node_id)?;
        Ok(node_type)
    }

    /// Creates a connection between two nodes in the same or different graphs.
    pub fn connect(&mut self, edge: Edge) -> Result<()> {
        if edge.slot_address_output.dag_id == edge.slot_address_input.dag_id {
            self.blueprint_dag_mut(edge.slot_address_output.dag_id)?
                .connect(
                    edge.slot_address_output.node_id,
                    edge.slot_address_output.slot_id,
                    edge.slot_address_input.node_id,
                    edge.slot_address_input.slot_id,
                )
        } else {
            let inter_dag_edge = edge.into();

            self.dag(edge.slot_address_output.dag_id)?;
            self.dag(edge.slot_address_input.dag_id)?;

            if self.inter_dag_edges.contains(&inter_dag_edge) {
                Err(VismutError::InvalidEdge)
            } else {
                self.inter_dag_edges.push(inter_dag_edge);
                Ok(())
            }
        }
    }

    /// Checks if an `Edge` can be created, returns useful error messages that can be used to decide
    /// what to do depending on what happens.
    pub fn can_connect(&mut self, edge: Edge) -> Result<()> {
        self.blueprint_dag_mut(edge.slot_address_output.dag_id)?
            .connection_valid(edge.into())
    }

    /// Returns a mutable reference to the `NodeType` at the address, if it exists.
    pub fn node_type_mut(&mut self, node_address: NodeAddress) -> Result<&mut NodeType> {
        let dag_id = node_address.dag_id;

        let dag = self
            .blueprint_dags
            .iter_mut()
            .find(|dag| dag.dag_id == dag_id)
            .ok_or(VismutError::InvalidDagId(dag_id))?;

        dag.dag.node_type_mut(node_address.node_id)
    }

    // todo: Why does the GUI need to be able to get output addresses?
    /// Returns the addresses of all output nodes.
    pub fn output_addresses(&self, dag_id: DagId) -> Result<Vec<SlotAddress>> {
        let node_slot_ids = self.dag(dag_id)?.output_addresses();
        let output_addresses = node_slot_ids
            .iter()
            .map(|(node_id, slot_id)| dag_id.with_node_id(*node_id).with_slot_id(*slot_id))
            .collect();
        Ok(output_addresses)
    }

    /// Returns the `Dag` with the given `DagId`.
    ///
    /// The `Dag` can be serialized.
    pub fn dag(&self, dag_id: DagId) -> Result<&Dag> {
        let dag = &self
            .blueprint_dags
            .iter()
            .find(|dag| dag.dag_id == dag_id)
            .ok_or(VismutError::InvalidDagId(dag_id))?
            .dag;

        Ok(dag)
    }

    pub(crate) fn blueprint_dag_mut(&mut self, dag_id: DagId) -> Result<&mut Dag> {
        let dag = &mut self
            .blueprint_dags
            .iter_mut()
            .find(|dag| dag.dag_id == dag_id)
            .ok_or(VismutError::InvalidDagId(dag_id))?
            .dag;

        Ok(dag)
    }

    /// Returns a new unique `DagId`.
    fn new_id(&mut self) -> DagId {
        let id = self.dag_counter;
        self.dag_counter += 1;
        DagId(id)
    }

    pub fn set_name(&mut self, node_address: NodeAddress, name: String) -> Result<()> {
        let dag = self.blueprint_dag_mut(node_address.dag_id)?;
        dag.set_name(node_address.node_id, name)
    }

    pub fn dag_edges(&self, dag_id: DagId) -> Result<Vec<Edge>> {
        let dag = self.dag(dag_id)?;

        let edges = dag
            .edges_iter()
            .map(|dag_edge| Edge {
                slot_address_output: SlotAddress {
                    dag_id,
                    node_id: dag_edge.node_id_output,
                    slot_id: dag_edge.slot_id_output,
                },
                slot_address_input: SlotAddress {
                    dag_id,
                    node_id: dag_edge.node_id_input,
                    slot_id: dag_edge.slot_id_input,
                },
            })
            .collect();

        Ok(edges)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn is_sync_send<T: Sync + Send>() {}

    #[test]
    fn engine_is_sync_send() {
        is_sync_send::<Engine>();
    }
}
