# Contributing to Vismut

To make contributing as easy and fun as possible,
we use our own lightweight process called the [Vismut Contribution Process](CONTRIBUTING.md).
Follow that link to learn how the project is structured,
and how you can get your contributions into the project.

Join the [Vismut Zulip](https://vismut.zulipchat.com) and ping `@**Lukas Orsvärn**`.
**He's more than happy to get on a video/voice call to show you around the code base and explain how things work**,
or help get you started with a task,
or discuss an idea or issue,
or do pair programming,
etc.
Working together enables a lot more learning,
fun,
and productivity!
😀

## Getting Started

This is a very broad overview of the flow of submitting code to be included in Vismut:

1. Clone the project
2. Make your changes
3. Fork the project on GitLab
   (you only need to do this once)
4. Push your changes to your fork
5. Create a merge request that merges your changes into Vismut
6. A maintainer will look at your merge request,
   and if it fills the criteria described in the [Vismut Contribution Process](CONTRIBUTING.md),
   it gets merged

If you get stuck,
join the [Vismut Zulip](https://vismut.zulipchat.com) and ask for help there.
You are welcome regardless of your level of experience,
and we are more than happy to teach you how things work.
😀
